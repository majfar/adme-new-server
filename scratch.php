<?php
/* ini_set('display_startup_errors',1);
ini_set('display_errors', 1);
ini_set('html_errors', 1);
error_reporting(E_ALL); */

$response = array();

if (isset($_REQUEST ['email']) && isset($_REQUEST['imei']) && isset($_REQUEST ['type']) && isset($_REQUEST ['auth_code'])) {

    date_default_timezone_set('America/New_York');


    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_response($msg);
        exit;
    }


    $imei = htmlentities(trim($_REQUEST['imei']));

    if($imei == ""){
        $msg = "Incomplete Device Information. Tablets are not allowed.";
        send_response($msg);

        exit;
    }

    $auth_code = htmlentities(trim($_REQUEST['auth_code']));

    $amount = "";


    if(isset($_REQUEST['amount'])){
        $amount = htmlentities(trim($_REQUEST['amount']));
    }

    $type = "";


    if(isset($_REQUEST['type'])){
        $type = htmlentities(trim($_REQUEST['type']));
    }

    $country = "us";
    if(isset($_REQUEST['country'])){
        $country = htmlentities(trim($_REQUEST['country']));
    }

    $ip = $_SERVER['REMOTE_ADDR'];

    /*switch($amount){
        case "0.500":
            $amount = 0.500;
            break;
        case "1.000":
            $amount= 1.000;
            break;
        case "2.000":
            $amount= 2.000;
            break;
        case "5.000":
            $amount= 5.000;
            break;
        case "10.000":
            $amount= 10.000;
            break;
        case "20.000":
            $amount= 20.000;
            break;
        case "50.000":
            $amount= 50.000;
            break;
        case "100.000":
            $amount= 100.000;
            break;
        case "200.000":
            $amount= 200.000;
            break;
        default:
            $msg = "Invalid information";
            send_response($msg);
            exit;
    }*/

    $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
    $settings_select->bind_param('s',$country);
    $settings_select->execute();

    if($settings_select->errno){
        $msg = "Country information unavailable. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $results = $settings_select->get_result();

    if($results->num_rows > 0){
        $settings = $results->fetch_assoc();
    }else{
        $msg = "Invalid country. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }


    $scratch_select = $db->prepare("SELECT * FROM scratch WHERE reward_amount = ?");
    $scratch_select->bind_param('d',$amount);
    $scratch_select->execute();

    if($scratch_select->errno){
        $msg = "Scratch card not found.";
        send_response($msg);

        $scratch_select->close();
        exit;
    }

    $results = $scratch_select->get_result();

    if($results->num_rows > 0){
        $scratch = $results->fetch_assoc();
    }else{
        $msg = "Sorry, Scratch is not valid.";
        send_response($msg);

        $scratch_select->close();
        exit;
    }

    $scratch_select->close();


    $now = new DateTime('now');

    if($type == "auth"){
        if($scratch['locked'] == 1){

            $date = new DateTime($scratch['eligibility_start_time']);

            $seconds1 = timeToSeconds($date->format('G:i:s'));
            $seconds2 = timeToSeconds($now->format('G:i:s'));
            $secondsBetween = $seconds2 - $seconds1;

            if($secondsBetween >= ($scratch['win_chance_time_limit']*60)){
                $scratch['locked'] = 0;
            }
        }

        if(($scratch['count_since_last_win']<$scratch['min_count_for_eligibility']) || ($scratch['locked'] == 1)){
            // the user is not going to receive a winning ticket
            //all we have to do is increment their counter
            $prepare_scratch = $db->prepare("UPDATE scratch SET total_distributed = total_distributed + 1, locked=? WHERE id=?");
            $prepare_scratch->bind_param('ii',$scratch['locked'],$scratch['id']);
            $prepare_scratch->execute();

            $prepare = $db->prepare("UPDATE accounts SET scratches_authorized = scratches_authorized + 1, last_ip=? WHERE email=? AND imei=? AND auth_code=?");
            $prepare->bind_param('sssi',$ip,$email,$imei,$auth_code);
            $prepare->execute();

            $msg = "0";
            send_response($msg);
        }else{
            //this is a winner so we need to let the user know
            //also need to mark it in the db
            $select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ? AND auth_code=?");
            $select_account->bind_param('ssi',$email,$imei,$auth_code);
            $select_account->execute();

            $results = $select_account->get_result();

            if($results->num_rows > 0){
                $account = $results->fetch_assoc();


                $prepare_scratch = $db->prepare("UPDATE scratch SET total_distributed = total_distributed + 1, locked = 1, current_eligible_user_id=?,
                                            eligibility_start_time=? WHERE id=?");
                $prepare_scratch->bind_param('isi',$account['id'],$now->format("Y-m-d H:i:s "),$scratch['id']);
                $prepare_scratch->execute();


                $prepare = $db->prepare("UPDATE accounts SET scratches_authorized = scratches_authorized + 1, last_ip=? WHERE email=? AND imei=? AND auth_code=?");
                $prepare->bind_param('sssi',$ip,$email,$imei,$auth_code);
                $prepare->execute();

                $msg="1";

                send_response($msg, $scratch['win_chance_time_limit']);
            }else{
                $msg = "Account not found";
                send_response($msg);
            }

        }


    }else if ($type == "view"){
        //user has viewed the ticket that he was given, not a winning ticket

        $bonus = $settings['scratch_bonus'];

        $update_scratch = $db->prepare("UPDATE scratch SET 
												count_since_last_win=count_since_last_win + 1,total_scratched=total_scratched+1
												WHERE id=?");
        $update_scratch->bind_param('i',$scratch['id']);
        $update_scratch->execute();

        $update_scratch->close();

        $update_account = $db->prepare("UPDATE accounts SET scratches = scratches + 1, last_ip=?, points_today_bonus=points_today_bonus+?
												WHERE email=? AND imei=? AND auth_code=?");
        $update_account->bind_param('sdssi',$ip,$bonus,$email,$imei,$auth_code);
        $update_account->execute();
        $update_account->close();

        $msg="1";
        $details = "This was not a winning ticket. However, you have received a bonus of $".$bonus." just for playing! Your account will be updated within 24 hours.";

        send_response($msg,0,0,"",$details);

    }else if ($type == "win"){
        //user was told that he was eligible to win and it seems that he scratched a winning ticket
        //here is where we do the logic to verify that he or she indeed is a winner
        $select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ? AND auth_code=?");
        $select_account->bind_param('ssi',$email,$imei,$auth_code);
        $select_account->execute();

        $results = $select_account->get_result();

        if($results->num_rows > 0){
            $account = $results->fetch_assoc();

            $code = generateWinCode();

            if($scratch['current_eligible_user_id'] == $account['id']) {

                $winner_history = '[{"date":"' . $now->format('F j, Y') . '","user":"' . $account['id'] . '","code":"' . $code . '"}]';

                if ($scratch['winner_history'] != NULL && $scratch['winner_history'] != "null") {
                    $prev_json = (array)json_decode($scratch['winner_history']);
                    $next_json = (array)json_decode($winner_history, true);
                    /* echo print_r($prev_json);
                    echo print_r($next_json); */
                    $array = array_merge($next_json, $prev_json);
                    $winner_history = json_encode($array);
                }
                $scratch['winner_history'] = $winner_history;

                $update_scratch = $db->prepare("UPDATE scratch SET locked=0, current_eligible_user_id=0, winner_history=?, 
												count_since_last_win=0,total_scratched=total_scratched+1,wins=wins+1
												WHERE id=?");
                $update_scratch->bind_param('si', $scratch['winner_history'], $scratch['id']);
                $update_scratch->execute();
                $update_scratch->close();


                $account['points_today_bonus'] = $account['points_today_bonus'] + $scratch['reward_amount'];

                $scratch_wins = '[{"date":"' . $now->format('F j, Y') . '","amount":"$' . $scratch['reward_amount'] . '","code":"' . $code . '"}]';
                if ($account['scratch_wins'] != NULL && $account['scratch_wins'] != "null") {
                    $prev_json = (array)json_decode($account['scratch_wins']);
                    $next_json = (array)json_decode($scratch_wins, true);
                    /* echo print_r($prev_json);
                    echo print_r($next_json); */
                    $array = array_merge($next_json, $prev_json);
                    $scratch_wins = json_encode($array);
                }
                $account['scratch_wins'] = $scratch_wins;
                $update_account = $db->prepare("UPDATE accounts SET scratches = scratches + 1, last_ip=?,points_today_bonus=?,scratch_wins=?
												WHERE email=? AND imei=? AND auth_code=?");
                $update_account->bind_param('sdsssi', $ip, $account['points_today_bonus'], $account['scratch_wins'], $email, $imei, $auth_code);
                $update_account->execute();
                $update_account->close();

                $msg = "1";
                send_response($msg, 0, 0, $code);
            }else{
                $msg = "0";
                send_response($msg);
            }

        }else{
            $msg = "Account not found";
            send_response($msg);
        }

    }

    $db->close();

}else{
    $msg = "Information missing. Something is wrong with your device.";
    send_response($msg);
}

function timeToSeconds($time){
    $timeExpanded = explode(':', $time);
    if (isset($timeExpanded[2])) {
        return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60 + $timeExpanded[2];
    }
    return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60;
}

function send_response($msg = "",$time=0,$balance=0,$code="",$details="") {
    $response["message"] = $msg;
    $response["time"] = $time;
    $response["code"] = $code;
    $response["balance"] = $balance;
    $response["details"] = $details;

    echo json_encode($response);
}

function generateWinCode() {
    $length = 4;
    $chars = array_merge ( range ( 0, 9 ), range ( 'A', 'Z' ) );
    shuffle ( $chars );
    $password = implode ( array_slice ( $chars, 0, $length ) );
    return $password;
}

?>