<?php


$email;
$imei;

//phpinfo();

//elg is eligible or not...

if (isset($_REQUEST ['email']) && isset($_REQUEST['imei'])) {
    date_default_timezone_set('America/New_York');


    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: " . mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_error($msg);
        exit;
    }


    $imei = htmlentities(trim($_REQUEST['imei']));

    if ($imei == "") {
        $msg = "Incomplete Device Information. Tablets are not allowed.";
        send_error($msg);

        exit;
    }

    $select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ?");
    $select_account->bind_param('ss',$email,$imei);
    $select_account->execute();

    $results = $select_account->get_result();

    if($results->num_rows > 0) {
        $account = $results->fetch_assoc();
        sendResponse($account['rewards_history']);
    }else{
        $msg = "Account not found.";
        send_error($msg);
    }

}else{
    $msg = "Information incomplete.";
    send_error($msg);
}

function send_error($msg = "") {
    $response["success"] = 0;
    $response["message"] = $msg;

    echo json_encode($response);
}

function sendResponse($history){
    $response["success"] = 1;
    $response["message"] = $history;

    echo json_encode($response);
}