<?php
 

$email;
$imei;

//phpinfo();

//elg is eligible or not...

if (isset($_REQUEST ['email']) && isset($_REQUEST['imei']) && isset($_REQUEST['method']) && isset($_REQUEST['elg'])) {
	date_default_timezone_set('America/New_York');
	
	
	 $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
	if ($db->connect_errno) {
		echo "Sorry, this website is experiencing problems.";
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $mysqli->connect_errno . "\n";
		echo "Error: " . $mysqli->connect_error . "\n";
		exit;
	}	

	$email = strtolower(htmlentities(trim($_REQUEST['email'])));
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
		$msg = "Email is not valid.";
		send_error($msg);
		exit;
	 }
	
	
	$imei = htmlentities(trim($_REQUEST['imei']));
	
	if($imei == ""){
		$msg = "Incomplete Device Information. Tablets are not allowed.";
		send_error($msg);
		
		exit;
	}
	
	$method = htmlentities(trim($_REQUEST['method']));
	
	if($method == ""){
		$msg = "Incomplete information provided.";
		send_error($msg);
		
		exit;
	}
	
	$country = "";
	$appversioncode = 0;
	$androidversion = 0;
	$locationenabled = 0;
	$phoneaccessenabled = 0;
	$eligible = false;
	
	if(isset($_REQUEST['country'])){
		$country = strtolower(htmlentities(trim($_REQUEST['country'])));
	}
	
	if(isset($_REQUEST['androidversion'])){
		$androidversion = htmlentities(trim($_REQUEST['androidversion']));
	}
	
	if(isset($_REQUEST['appversioncode'])){
		$appversioncode = htmlentities(trim($_REQUEST['appversioncode']));
	}
	
	if(isset($_REQUEST['locationenabled'])){
		$locationenabled = htmlentities(trim($_REQUEST['locationenabled']));
	}
	
	if(isset($_REQUEST['phoneaccessenabled'])){
		$phoneaccessenabled = htmlentities(trim($_REQUEST['phoneaccessenabled']));
	}
	
	if(isset($_REQUEST['elg'])){
		$eligible = htmlentities(trim($_REQUEST['elg']));
	}
	
	
	$ip = $_SERVER['REMOTE_ADDR'];
	
	$now = new DateTime('now');
	$now = date('Y-m-d G:i:s');
	
	$settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
	$settings_select->bind_param('s',$country);
	$settings_select->execute();
	
	if($settings_select->errno){
		$msg = "Country information unavailable. Enable GPS.";
		send_error($msg);
		
		$settings_select->close();
		exit;
	}
	
	$results = $settings_select->get_result();
	
	if($results->num_rows > 0){
		$settings = $results->fetch_assoc();
	}else{
		$msg = "Invalid country. Enable GPS.";
		send_error($msg);
		
		$settings_select->close();
		exit;
	}
	
	
	/* 
		1. Pull up the account associated with the user
		2. Get all associated parameters and update them based on latest query
		3. Update the number of unlocks and the last_activity_date
		4. Update the days_active date (if it's a new day)
		5. Confirm that the user is not banned and that the yearly and lifetime caps are not met & appversioncode is above the minimum
			a. Get the last_earned_time
			b. Get the number of minutes since last earned.
			c. Check if the number of minutes exceeds the time set in Settings and the points for the day hasn't exceeded the daily cap and if so update:
				i. current_points
				ii. current points this year
				iii. total points earned.
		6. Send back a response
	
	*/
	
	
	$select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ?");
	$select_account->bind_param('ss',$email,$imei);
	$select_account->execute();
	
	$results = $select_account->get_result();
	
	if($results->num_rows > 0){
		$account = $results->fetch_assoc();
		
		/* Step 2 */
		
		$account['country'] = $country;
		$account['appversioncode'] = $appversioncode;
		$account['androidversion'] = $androidversion;
		$account['location_enabled'] = $locationenabled;
		$account['phoneaccess_enabled'] = $phoneaccessenabled;
		$account['last_ip'] = $ip;
		
		
		/* Step 3 */
		$account['num_unlocks'] += 1;
				
		$now = new DateTime('now');
		$account['last_activity_date'] = date('Y-m-d G:i:s');
		
		if($method == "slide"){
			$account['unlock_method_slide'] += 1;
		}else if($method == "quicklaunch"){
			$account['unlock_method_ql'] += 1;
		}else if($method == "homebutton"){
			$account['unlock_method_home'] += 1;
		}else{
			$msg = "Invalid unlock method";
			send_response($msg);
			
			exit;
		}

		
		/* Step 4 */
		$date = new DateTime($account['points_lastearned']);
		$day1 = $date->format('d');
		$day2 = $now->format('d');
		$daysBetween = $day2 - $day1;
		
		$year1 = $date->format('y');
		$year2 = $now->format('y');
		$yearsBetween = $year2 - $year1;
		
		$seconds1 = timeToSeconds($date->format('G:i:s'));
		$seconds2 = timeToSeconds($now->format('G:i:s'));
		$secondsBetween = $seconds2 - $seconds1;
	
		if($daysBetween != 0 && ($account['points_today'] > 0 || $account['points_today_bonus'] > 0)){
			$account['days_active'] += 1;

			if($account['days_active']==10){//special things to do on the 10th day of a user being active

            }
			
			
			$account['points_now'] += $account['points_today'];
			$account['points_now'] += $account['points_today_bonus']; //this means that bonus points only get added once a day AFTER an unlock occurs
			$account['points_year'] += $account['points_today'];
			$account['points_lifetime'] += $account['points_today'];
			$account['points_lifetime'] += $account['points_today_bonus'];


            $yesterday = new DateTime();
            $yesterday->add(DateInterval::createFromDateString('yesterday'));
			
			
			$rewards_history = '[{"date":"'.$yesterday->format('F j, Y').'","amount":"$'.($account['points_today']+$account['points_today_bonus']).'"}]';
			
			if($account['rewards_history'] != NULL && $account['rewards_history'] != "null"){
				$prev_json = (array)json_decode($account['rewards_history']);
				$next_json = (array)json_decode($rewards_history,true);
				/* echo print_r($prev_json);
				echo print_r($next_json); */
				$array = array_merge($next_json,$prev_json);
				$rewards_history = json_encode($array);
			}			
			$account['rewards_history'] = $rewards_history;
			
			
			
			
			$account['points_today'] = 0;
			$account['points_today_bonus'] = 0;
			$account['num_unlocks_today'] = 0;
			$account['num_eligible_unlocks_today'] = 0;
			
		}
		
		$account['num_unlocks_today'] += 1;
		/*if($eligible == "true"){*/
			$account['num_eligible_unlocks_today'] += 1;
			$account['num_eligible_unlocks'] += 1;
		/*}*/
		
		
		/* Step 5 */
		
		if(!$account['banned']){
			if($yearsBetween >= 1){
				$accounts['points_year'] = 0;
			}
			
			
			if($account['appversioncode'] < $settings['min_version']){
				sendResponse($db, $account,"YSYL"); //You snooze you lose
				
				$select_account->close();
				exit;				
			}
			
			if($account['points_year'] >= $settings['points_cap_year']){
				sendResponse($db, $account, "YCHBH"); // Yearly cap has been hit
				
				$select_account->close();
				exit;
			}
			
			
			if ($daysBetween == 0){
                $cap = $settings['points_cap_day'];

                if($account['appmode'] == 2 || $account['appmode']==3){
                    $cap = ($cap / 2) - 0.01;
                }

				if($account['points_today'] >= $cap){
					sendResponse($db, $account, "DCHBH"); //Daily cap has been hit
					
					$select_account->close();
			
					exit;
				}
				
				if (($secondsBetween < ($settings['points_frequency'] * 60)) || ($secondsBetween < 0)) {
					sendResponse($db, $account,"CBL"); //Come back later
					$select_account->close();
					
					exit;
				}				
			}
			
			if($eligible == "true"){
			$increment = $settings['points_rate'];
			
			$account['points_today'] += $increment;
			
			$now = new DateTime('now');
			$now = date('Y-m-d G:i:s');
			$account['points_lastearned'] = $now;
			}
			
			sendResponse($db, $account, "EYTT"); //Enjoy your tasty treats
			$select_account->close();
			
			exit;
			
		}else{
			sendResponse($db, $account, "GLPOJ"); //Get lost, piece of junk
			$select_account->close();
			exit;
		}
		
		
		
	}else{
		$msg = "Unable to find account.";
		send_error($msg);
		
		$select_account->close();
	}
} else{
	$msg = "Information incomplete.";
	send_error($msg);
}

function timeToSeconds($time){
     $timeExpanded = explode(':', $time);
     if (isset($timeExpanded[2])) {
         return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60 + $timeExpanded[2];
     }
     return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60;
}



function updateAccount($db, $account){
	$update_account = $db->prepare("UPDATE accounts SET country=?, appversioncode=?, androidversion=?, location_enabled=?, phoneaccess_enabled=?,
									last_ip=?, num_unlocks=?, last_activity_date=?, unlock_method_slide=?, unlock_method_home=?,unlock_method_ql=?, 
									days_active=?, points_now=?,points_year=?, points_lifetime=?, rewards_history=?,
									points_today=?,points_today_bonus=?,num_unlocks_today=?,points_lastearned=?,num_eligible_unlocks_today=?,num_eligible_unlocks=?
									WHERE email=? AND imei=?");
	$update_account->bind_param('sisiisisiiiidddsddisiiss',$account['country'],$account['appversioncode'],$account['androidversion'],
											$account['location_enabled'],$account['phoneaccess_enabled'],$account['last_ip'],$account['num_unlocks'],
											$account['last_activity_date'],$account['unlock_method_slide'],$account['unlock_method_home'],
											$account['unlock_method_ql'],$account['days_active'],$account['points_now'],
											$account['points_year'],$account['points_lifetime'],$account['rewards_history'],
											$account['points_today'],$account['points_today_bonus'],$account['num_unlocks_today'],$account['points_lastearned'],
											$account['num_eligible_unlocks_today'],$account['num_eligible_unlocks'],
											$account['email'],$account['imei']);
	$update_account->execute();

	if($update_account->errno){
		echo $update_account->error;
		$msg = "Couldn't save changes";
		send_error($msg);
		
		$update_account->close();
		exit;
	}
	
	$update_account->close();	
}

function send_error($msg = "") {
		$response["success"] = 0;
        $response["message"] = $msg;
		
		echo json_encode($response);		
    }
	
function sendResponse($db, $account, $msg = ""){
	updateAccount($db, $account);


	$response["success"] = 1;
    $response["message"] = $msg;
	
	echo json_encode($response);	
}
	

?>