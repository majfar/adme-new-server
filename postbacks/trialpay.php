<?php
 

$ip = $_SERVER['REMOTE_ADDR'];

if ( $ip == "54.183.233.157" || $ip == "54.183.231.95" || ip_in_range($ip, "70.42.249.1/32") || ip_in_range($ip,"199.68.159.0/32")){
  
    $message_signature = $_SERVER['HTTP_TRIALPAY_HMAC_MD5'];
	$key = 'ce93f2c68b';
	
	if (empty($HTTP_RAW_POST_DATA)) {
	$recalculated_message_signature = hash_hmac('md5', file_get_contents('php://input'), $key);
	} else {
	$recalculated_message_signature = hash_hmac('md5', $HTTP_RAW_POST_DATA, $key);
	}
	
	if ($message_signature == $recalculated_message_signature) {
	 
		$email = $_POST['email'];
		$auth = $_POST['sid'];
		$country = $_POST['country'];
		$reward = $_POST['reward_amount'];

		date_default_timezone_set('America/New_York');

        $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
        if ($db->connect_errno) {
            echo "Sorry, this website is experiencing problems.";
            echo "Error: Failed to make a MySQL connection, here is why: \n";
            echo "Errno: " . $mysqli->connect_errno . "\n";
            echo "Error: " . $mysqli->connect_error . "\n";
            exit;
        }

        if($reward > 20000){//too many points
			exit;
		}

        $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
        $settings_select->bind_param('s',$country);
        $settings_select->execute();

        if($settings_select->errno){
            echo "no country";
            $settings_select->close();
            exit;
        }

        $results = $settings_select->get_result();

        if($results->num_rows > 0){
            $settings = $results->fetch_assoc();
        }else{
            exit;
        }
		 
		$select_account = $db->prepare("SELECT * FROM accounts WHERE auth_code = ?");
		$select_account->bind_param('i',$auth);
		$select_account->execute();
		
		$results = $select_account->get_result();
		
		if($results->num_rows == 0){
		  //  echo "invalid auth";
			exit;
		}
		
		$account = $results->fetch_assoc();
		
		$select_account->close();
		
		$now = new DateTime('now');
		$now = date('Y-m-d G:i:s');

        $money = floor_dec(($reward / $settings['trialpay_exchange_rate']));

		$account['trialpay_pts_added'] += $reward;
		$account['trialpay_count'] += 1;
		$account['points_today_bonus'] += $money;
		
		$update_account = $db->prepare("UPDATE accounts SET points_today_bonus=?,
									trialpay_pts_added=?, trialpay_count=?, points_lastearned=?
									WHERE auth_code=?");
		$update_account->bind_param('diisi',$account['points_today_bonus'],
											$account['trialpay_pts_added'],$account['trialpay_count'],$now,$auth);
        if($update_account->execute()){
            echo "1";
        }else{
            echo $update_account->error;
        }
		
		$update_account->close();
	} else {
	    echo "error";
	}
	
}else{
	echo "not authorized";
}

function ip_in_range( $ip, $range ) {
    if ( strpos( $range, '/' ) == false ) {
        $range .= '/32';
    }
    // $range is in IP/CIDR format eg 127.0.0.1/24
    list( $range, $netmask ) = explode( '/', $range, 2 );
    $range_decimal = ip2long( $range );
    $ip_decimal = ip2long( $ip );
    $wildcard_decimal = pow( 2, ( 32 - $netmask ) ) - 1;
    $netmask_decimal = ~ $wildcard_decimal;
    return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
}

function floor_dec($number,$precision = 3,$separator = '.') {
    $numberpart=explode($separator,$number);
    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
    if($numberpart[0]>=0) {
        $numberpart[1]=substr(floor('1'.$numberpart[1]),1);
    } else {
        $numberpart[1]=substr(ceil('1'.$numberpart[1]),1);
    }
    $ceil_number= array($numberpart[0],$numberpart[1]);
    return implode($separator,$ceil_number);
}
?>