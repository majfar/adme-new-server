<?php
 

$ip = $_SERVER['REMOTE_ADDR'];
	
if ( $ip == "204.232.224.18" || $ip == "204.232.224.19" || $ip == "104.130.46.116" || $ip == "104.130.60.109" || $ip == "104.239.224.178" || $ip == "104.130.60.108") {
  
	$auth = $_GET['sub1'];
	$country = $_GET['sub2'];

	
	date_default_timezone_set('America/New_York');

    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

	 
	$pts = round(abs($_GET['rate']),0,PHP_ROUND_HALF_DOWN); // can be negative is the survey is reversed
	
	if($pts > 20000){//too many points
		exit;
	}
	
	$status = $_GET['status'];

    $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
    $settings_select->bind_param('s',$country);
    $settings_select->execute();

    if($settings_select->errno){
        $msg = "Country information unavailable. Enable GPS.";
       // send_error($msg);
        echo $msg;

        $settings_select->close();
        exit;
    }

    $results = $settings_select->get_result();

    if($results->num_rows > 0){
        $settings = $results->fetch_assoc();
    }else{
      //  echo "no result";
        exit;
    }
	 
	$select_account = $db->prepare("SELECT * FROM accounts WHERE auth_code = ?");
	$select_account->bind_param('i',$auth);
	$select_account->execute();
	
	$results = $select_account->get_result();
	
	if($results->num_rows == 0){
	   // echo "account not found";
		exit;
	}
	
	$account = $results->fetch_assoc();
	
	$select_account->close();
	
	$now = new DateTime('now');
	$now = date('Y-m-d G:i:s');

	$money = floor_dec(($pts / $settings['adscend_exchange_rate']));
	
	switch($status) {
		case 1 :
			$account['adscend_pts_added'] += $pts;
			$account['adscend_survey_count'] += 1;
			$account['points_today_bonus'] += $money;
			break;
		case 2:
			$account['adscend_pts_removed'] += $pts;
			$account['ponts_now'] -= $money;
			$account['points_year'] -= $money;
			$account['points_lifetime'] -= $money;
			break;		
	}
	
	$update_account = $db->prepare("UPDATE accounts SET points_today_bonus = ?, points_now=?, points_year=?, points_lifetime=?,
								adscend_pts_added=?, adscend_pts_removed=?, adscend_survey_count=?, points_lastearned=?
								WHERE auth_code=?");
	$update_account->bind_param('ddddiiisi',$account['points_today_bonus'],$account['points_now'],$account['points_year'],$account['points_lifetime'],
										$account['adscend_pts_added'],$account['adscend_pts_removed'],$account['adscend_survey_count'],$now,$auth);
	if($update_account->execute()){
	    echo "success";
    }else{
	   // echo $update_account->error;
     //   echo "fail";
    }
	
	$update_account->close();
}

function floor_dec($number,$precision = 3,$separator = '.') {
    $numberpart=explode($separator,$number);
    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
    if($numberpart[0]>=0) {
        $numberpart[1]=substr(floor('1'.$numberpart[1]),1);
    } else {
        $numberpart[1]=substr(ceil('1'.$numberpart[1]),1);
    }
    $ceil_number= array($numberpart[0],$numberpart[1]);
    return implode($separator,$ceil_number);
}

?>