
<?php

/* Your postback url:
https://server.surveycow.co/postbacks/pollfish.php
?device_id=[[device_id]]&cpa=[[cpa]]&timestamp=[[timestamp]]
&request_uuid=[[request_uuid]]&signature=[[signature]] */

	  $secret_key = "b4c8b35f-20c4-4364-abf5-7d42dede9294";

	  $cpa = $_GET["cpa"];
	  $device_id = $_GET["device_id"];
	  $request_uuid = $_GET["request_uuid"];
	  $timestamp = $_GET["timestamp"];
	  //$tx_id = urldecode($_GET["tx_id"]);
	  $url_signature = $_GET["signature"];

	  $data = $cpa . ":" . $device_id. ":" . $request_uuid. ":" . $timestamp;

	  $computed_signature = base64_encode(hash_hmac("sha1" , $data, $secret_key, true));
	  $is_valid = $url_signature == $computed_signature;
	  
		/* <p>cpa = <?php echo($cpa); ?></p>
		<p>device_id = <?php echo($device_id); ?></p>
		<p>request_uuid = <?php echo($request_uuid); ?></p>
		<p>timestamp = <?php echo($timestamp); ?></p>
		<p>tx_id = <?php echo($tx_id); ?></p>

		<p>The signature is generated using the input string:<code><?php echo($data); ?></code></p>

		<p>The generated signature is: <?php echo($computed_signature); ?></p>
		<p>The URL signature is: <?php echo($url_signature); ?></p>
		<p>The signature is valid: <?php echo($is_valid ? "true" : "false"); ?></p> */
	 
	if($is_valid){
		date_default_timezone_set('America/New_York');

        $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
        if ($db->connect_errno) {
            echo "Sorry, this website is experiencing problems.";
            echo "Error: Failed to make a MySQL connection, here is why: \n";
            echo "Errno: " . $mysqli->connect_errno . "\n";
            echo "Error: " . $mysqli->connect_error . "\n";
            exit;
        }

		 
		$pts = round(($cpa*5),0,PHP_ROUND_HALF_DOWN);
		
		if($pts > 20000){//too many points
			echo "Illegal";
			exit;
		}
		
		 
		$select_account = $db->prepare("SELECT * FROM accounts WHERE auth_code = ?");
		$select_account->bind_param('i',$request_uuid);
		$select_account->execute();
		
		$results = $select_account->get_result();
		
		if($results->num_rows == 0){
			echo "request id is wrong";
			exit;
		}
		
		$account = $results->fetch_assoc();
		
		$select_account->close();
		
		$now = new DateTime('now');
		$now = date('Y-m-d G:i:s');

		$money = floor_dec(($pts/1000));
		
		$account['pollfish_pts_added'] += $money;
		$account['pollfish_survey_count'] += 1;
		$account['points_today_bonus'] += $money;
		
		$update_account = $db->prepare("UPDATE accounts SET points_today_bonus=?,
									pollfish_survey_count=?, pollfish_pts_added=?, points_lastearned=?
									WHERE auth_code=?");
		$update_account->bind_param('didsi',$account['points_today_bonus'],
											$account['pollfish_survey_count'],$account['pollfish_pts_added'],$now,$request_uuid);
        if($update_account->execute()){
            echo "success";
        }else{
            echo $update_account->error;
        }


        $update_account->close();
	}else{
		echo "invalid";
	}

function floor_dec($number,$precision = 3,$separator = '.') {
    $numberpart=explode($separator,$number);
    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
    if($numberpart[0]>=0) {
        $numberpart[1]=substr(floor('1'.$numberpart[1]),1);
    } else {
        $numberpart[1]=substr(ceil('1'.$numberpart[1]),1);
    }
    $ceil_number= array($numberpart[0],$numberpart[1]);
    return implode($separator,$ceil_number);
}
?>