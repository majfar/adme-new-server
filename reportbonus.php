<?php

 /*ini_set('display_startup_errors',1);
ini_set('display_errors', 1);
ini_set('html_errors', 1);
error_reporting(E_ALL);*/

$email;
$imei;

//phpinfo();

if (isset($_REQUEST ['email']) && isset($_REQUEST['imei']) && isset($_REQUEST['auth_code']) && isset($_REQUEST['country'])) {
    date_default_timezone_set('America/New_York');


    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_error($msg);
        exit;
    }


    $imei = htmlentities(trim($_REQUEST['imei']));

    if($imei == ""){
        $msg = "Incomplete Device Information. Tablets are not allowed.";
        send_error($msg);

        exit;
    }

    $country = htmlentities(trim($_REQUEST['country']));

    $auth = htmlentities(trim($_REQUEST['auth_code']));

    $ip = $_SERVER['REMOTE_ADDR'];

    $now = new DateTime('now');

    $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
    $settings_select->bind_param('s',$country);
    $settings_select->execute();

    if($settings_select->errno){
        $msg = "Country information unavailable. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $results = $settings_select->get_result();

    if($results->num_rows > 0){
        $settings = $results->fetch_assoc();
    }else{
        $msg = "Invalid country. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    /*
        1. Pull up the account associated with the user
        2. Get all associated parameters and update them based on latest query
        3. Update the number of unlocks and the last_activity_date
        4. Update the days_active date (if it's a new day)
        5. Confirm that the user is not banned and that the yearly and lifetime caps are not met & appversioncode is above the minimum
            a. Get the last_earned_time
            b. Get the number of minutes since last earned.
            c. Check if the number of minutes exceeds the time set in Settings and the points for the day hasn't exceeded the daily cap and if so update:
                i. current_points
                ii. current points this year
                iii. total points earned.
        6. Send back a response

    */


    $select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ? AND auth_code=?");
    $select_account->bind_param('ssi',$email,$imei,$auth);
    $select_account->execute();

    $results = $select_account->get_result();

    if($results->num_rows > 0) {
        $account = $results->fetch_assoc();

        if ($account['banned']) {
            send_error("GLPOJ"); //Get lost, piece of junk
            $select_account->close();
            exit;
        }

        $account['country'] = $country;
        $account['last_ip'] = $ip;

        if ($account['bonus_lastvisited'] == NULL) {
            $daysBetween =1;
        }else{

        $date = new DateTime($account['bonus_lastvisited']);
        $day1 = $date->format('d');
        $day2 = $now->format('d');
        $daysBetween = $day2 - $day1;

        $year1 = $date->format('y');
        $year2 = $now->format('y');
        $yearsBetween = $year2 - $year1;

        $seconds1 = timeToSeconds($date->format('G:i:s'));
        $seconds2 = timeToSeconds($now->format('G:i:s'));
        $secondsBetween = $seconds2 - $seconds1;
        }

        if($daysBetween != 0){

            $account['points_today_bonus'] += $settings[bonus_page_bonus];

            $now = new DateTime('now');
            $now = date('Y-m-d G:i:s');
            $account['bonus_lastvisited'] = $now;
            $account['last_activity_date'] = $now;

            sendResponse($db, $account, "EYTT",$settings['bonus_page_bonus']); //Enjoy your tasty treats
            $select_account->close();

            exit;

        }else{
            send_error("CMT"); //Come back tomorrow
            $select_account->close();
            exit;
        }

    }else{
        $msg = "Unable to find account.";
        send_error($msg);

        $select_account->close();
    }
} else{
    $msg = "Information incomplete.";
    send_error($msg);
}

function timeToSeconds($time){
    $timeExpanded = explode(':', $time);
    if (isset($timeExpanded[2])) {
        return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60 + $timeExpanded[2];
    }
    return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60;
}



function updateAccount($db, $account){
    $update_account = $db->prepare("UPDATE accounts SET	country=?,last_ip=?,last_activity_date=?, points_today_bonus=?,bonus_lastvisited=?
									WHERE id=?");
    $update_account->bind_param('sssdsi',$account['country'],$account['last_ip'],$account['last_activity_date'],
        $account['points_today_bonus'],$account['bonus_lastvisited'],$account['id']);
    $update_account->execute();

    if($update_account->errno){
        echo $update_account->error;
        $msg = "Couldn't save changes";
        send_error($msg);

        $update_account->close();
        exit;
    }

    $update_account->close();
}

function send_error($msg = "") {
    $response["success"] = 0;
    $response["message"] = $msg;

    echo json_encode($response);
}

function sendResponse($db, $account, $msg = "",$amount=0){
    updateAccount($db, $account);


    $response["success"] = 1;
    $response["message"] = $msg;
    $response["amount"] = $amount;

    echo json_encode($response);
}


?>