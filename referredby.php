<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);
// array for JSON response
$response = array();*/

$email;
$imei;

if (isset($_REQUEST['email']) && isset($_REQUEST['imei']) && isset($_REQUEST['country']) && isset($_REQUEST['auth_code']) && isset($_REQUEST['promo_code']) ) {

    date_default_timezone_set('America/New_York');

    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_error($msg);
        exit;
    }

    $imei = htmlentities(trim($_REQUEST['imei']));

    $date = new DateTime('now');
    $date->setTimezone(new DateTimeZone('America/New_York'));
    $str_server_today = $date->format('YmdYdm');
    $today = intval($str_server_today);

    $auth_code = intval(htmlentities(trim($_REQUEST['auth_code'])));

    $country = strtolower(htmlentities(trim($_REQUEST['country'])));

    $promo_code = htmlentities(trim($_REQUEST['promo_code']));

    if($promo_code == ""){
        $msg = "Bonus code is missing.";
        send_error($msg);
        exit;
    }

    $select_referred_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ? AND auth_code=?");
    $select_referred_account->bind_param('ssi',$email,$imei,$auth_code);
    $select_referred_account->execute();

    $results = $select_referred_account->get_result();

    if($results->num_rows == 0){
        $msg = "We couldn't find your account.";
        send_error($msg);

        $select_referred_account->close();
        exit;
    }

    $referred_account = $results->fetch_assoc();
    $select_referred_account->close();

    if($referred_account['referredby'] != NULL
        || $referred_account['referredby'] != ""){
        $msg = "You've already entered a bonus code.";
        send_error($msg);

        exit;
    }

    if($referred_account['referrercode']==$promo_code){
        $msg = "Self referral is not allowed.";
        send_error($msg);
        exit;
    }

    $now_unformatted = new DateTime('now');
    $now_unformatted->setTimezone(new DateTimeZone('America/New_York'));
    $now = date('Y-m-d G:i:s');

    $select_referrer_account = $db->prepare("SELECT * FROM accounts WHERE referrercode = ?");
    $select_referrer_account->bind_param('s',$promo_code);
    $select_referrer_account->execute();

    $results = $select_referrer_account->get_result();

    $bonus;
    if($results->num_rows > 0){ //was referred by another user
        /*This portion of rewarding the referrer now gets done at redemption*/

        /*$referrer_account = $results->fetch_assoc();
        $select_referrer_account->close();
        if($referrer_account['banned'] == 1){
            $msg = "Referrer is banned.";
            send_error($msg);
            exit;
        }*/

        $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
        $settings_select->bind_param('s',$country);
        $settings_select->execute();

        if($settings_select->errno){
            $msg = "Country information unavailable. Enable GPS.";
            send_error($msg);

            $settings_select->close();
            exit;
        }

        $results = $settings_select->get_result();
        $settings_select->close();

        if($results->num_rows > 0){
            $settings = $results->fetch_assoc();
        }else{
            $msg = "Invalid country. Enable GPS.";
            send_error($msg);
            exit;
        }

        $bonus = $settings['referred_rate'];
        $referred_account['referredby'] = $promo_code;
        $referred_account['points_today_bonus'] += $bonus;


        /*$referrer_account['referralcount'] += 1;
        $referrer_account['points_today_bonus'] += $settings['referrer_rate'];

        $email = obfuscate_email($referred_account['email']);

        $referred_users = '[{"email":"'.$email.'","signup_date":"'.$now_unformatted->format('F j, Y').'"}]';

        if($referrer_account['referred_users'] != NULL && $referrer_account['referred_users'] != "null"){
            $prev_json = (array)json_decode($referrer_account['referred_users']);
            $next_json = (array)json_decode($referred_users,true);
            $array = array_merge($next_json,$prev_json);
            $referred_users = json_encode($array);
        }

        $update_referrer = $db->prepare("UPDATE accounts SET referralcount=?, points_today_bonus=?, referred_users=?,last_activity_date=?
								WHERE id=?");
        $update_referrer->bind_param('idssi',$referrer_account['referralcount'],$referrer_account['points_today_bonus'], $referred_users,
            $now,$referrer_account['id']);
        $update_referrer->execute();

        $update_referrer->close();*/
    }else{ //wasn't referred by another user
        $select_referrer_account->close();

        $promotion_select = $db->prepare("SELECT * FROM promotions WHERE promotion_code = ? AND country_code = ?");
        $promotion_select->bind_param('ss',$promo_code,$country);
        $promotion_select->execute();

        if($promotion_select->errno){
            $msg = "We were unable to find this user or promotion.";
            send_error($msg);

            $promotion_select->close();
            exit;
        }

        $results = $promotion_select->get_result();
        $promotion_select->close();

        if($results->num_rows > 0){
            $promotion = $results->fetch_assoc();
        }else{
            $msg = "We were unable to find this user or promotion.";
            send_error($msg);
            exit;
        }

        if($promotion['end_date'] != NULL){
            if($now > $promotion['end_date']){
                $msg = "This promotion has ended.";
                send_error($msg);
                exit;
            }
        }

        $bonus = $promotion['bonus'];
        $referred_account['referredby'] = $promo_code;
        $referred_account['points_today_bonus'] += $bonus;

        $promotion['usage_count'] += 1;

        $update_promotion = $db->prepare("UPDATE promotions SET usage_count=?, last_used=?
								WHERE id=?");
        $update_promotion->bind_param('isi',$promotion['usage_count'],
            $now,$promotion['id']);
        $update_promotion->execute();

        $update_promotion->close();

    }

    $update_referred = $db->prepare("UPDATE accounts SET referredby=?, points_today_bonus=?,last_activity_date=?
								WHERE id=?");
    $update_referred->bind_param('sdsi',$referred_account['referredby'],$referred_account['points_today_bonus'],
        $now,$referred_account['id']);
    $update_referred->execute();

    if($update_referred->errno){
        $msg = "An error occurred (UNSA). Email us at team@adme.mobi";
        send_error($msg);
        exit;
    }

    $update_referred->close();

    send_success($bonus);


}else{
    $msg = "Parameters are missing";
    send_error($msg);
}

function send_success($bonus) {
    $response["success"] = 1;
    $response["bonus"] = $bonus;
    $response["message"] = "Bonus successfully applied";

    echo json_encode($response);
}

function send_error($msg = "") {
    $response["success"] = 0;
    $response["message"] = $msg;

    echo json_encode($response);
}

function obfuscate_email($email){
    $em   = explode("@",$email);
    $name = implode(array_slice($em, 0, count($em)-1), '@');
    $len  = floor(strlen($name)/2);

    return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);
}
?>