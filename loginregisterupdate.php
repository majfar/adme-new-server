<?php

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
// array for JSON response
$response = array();

$email;
$imei;


//phpinfo();

if (isset($_REQUEST ['email']) && isset($_REQUEST['imei']) && isset($_REQUEST ['country'])) {

    date_default_timezone_set('America/New_York');


    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_error($msg);
        exit;
    }


    $imei = htmlentities(trim($_REQUEST['imei']));

    if($imei == ""){
        $msg = "Incomplete Device Information. Tablets are not allowed.";
        send_error($msg);

        exit;
    }


    $country = "";
    $deviceid = "";
    $referredby = "";
    $phonemake = "";
    $phonemodel = "";
    $androidversion = "";
    $appversioncode = 0;
    $locationenabled = 0;
    $phoneaccessenabled = 0;

    if(isset($_REQUEST['country'])){
        $country = strtolower(htmlentities(trim($_REQUEST['country'])));
    }

    if(isset($_REQUEST['deviceid'])){
        $deviceid = htmlentities(trim($_REQUEST['deviceid']));
    }

    if(isset($_REQUEST['referredby'])){
        $referredby = htmlentities(trim($_REQUEST['referredby']));
    }

    if(isset($_REQUEST['phonemake'])){
        $phonemake = htmlentities(trim($_REQUEST['phonemake']));
    }

    if(isset($_REQUEST['phonemodel'])){
        $phonemodel = htmlentities(trim($_REQUEST['phonemodel']));
    }

    if(isset($_REQUEST['androidversion'])){
        $androidversion = htmlentities(trim($_REQUEST['androidversion']));
    }

    if(isset($_REQUEST['appversioncode'])){
        $appversioncode = htmlentities(trim($_REQUEST['appversioncode']));
    }

    if(isset($_REQUEST['locationenabled'])){
        $locationenabled = htmlentities(trim($_REQUEST['locationenabled']));
    }

    if(isset($_REQUEST['phoneaccessenabled'])){
        $phoneaccessenabled = htmlentities(trim($_REQUEST['phoneaccessenabled']));
    }

    $ip = $_SERVER['REMOTE_ADDR'];



    //TODO sanitize all variables to make sure there's no SQL injection.

    /*
        1. Check the db to see if an account exists with the imei & email;
            a. If yes, then send the user the information for their account.
        2. If not previously matched check to see if an account exists with email
            a. this means the user has a new device.
            b. Update the account table for the new device.
            c. Keep a record of the old devices
            d. Send back account information to the user
        3. If no match found, check the old server to see if an account exists.
            a. if a match is found then this users account needs to be migrated.
            b. Take information from the old server and make a new account on the new server
            c. Send back account information to the user.
        4. If still no match, then this is a new account or an account being migrated from the old sever.
            a. Save all information in the account table.
            b. Save the device information in separate table (maybe).
            c. Send back account information to the user.

    */

    if($country == ""){
        $msg = "Country information unavailable. Enable GPS.";
        send_error($msg);
        exit;
    }


    $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
    $settings_select->bind_param('s',$country);
    $settings_select->execute();

    if($settings_select->errno){
        $msg = "Error getting country information.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $results = $settings_select->get_result();

    if($results->num_rows > 0){
        $settings = $results->fetch_assoc();
    }else{
        $msg = "Sorry, Adme is currently not available in your country.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $today = new DateTime('now');
    $now = date('Y-m-d G:i:s');

    //Step 1 - Send account info for existing user on the same device
    $step_1_update = $db->prepare("UPDATE accounts SET country=?, appversioncode=?, phonemake=?, phonemodel=?, androidversion=?, last_activity_date=? WHERE email=? AND imei=?");

    $step_1_update->bind_param('sissssss',$country,$appversioncode,$phonemake,$phonemodel,$androidversion,$now,$email,$imei);

    if($step_1_update->execute()){

        $step_1_select = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ?");
        $step_1_select->bind_param('ss',$email,$imei);
        $step_1_select->execute();

        $results = $step_1_select->get_result();

        $success = false;
        if($results->num_rows > 0){
            $result = $results->fetch_assoc();

            $msg = "Welcome back.";
            send_user($result, $settings, $msg);

            $success = true;
        }

        $step_1_select->close();
        $step_1_update->close();

        if($success){
            $db->close();
            exit;
        }
    }else{

        $step_1_update->close();
    }


    //Step 2 - Send account information for an existing user on a new device
    $step_2_select = $db->prepare("SELECT * FROM accounts WHERE email = ?");
    $step_2_select->bind_param('s',$email);
    $step_2_select->execute();

    $results = $step_2_select->get_result();

    if($results->num_rows > 0){
        $result = $results->fetch_assoc();
        $migration_count = $result["migration_count"] + 1;

        $device_history = '[{"imei":"'.$imei.'","date":"'.$today->format('F j, Y').'","phonemake":"'.$phonemake.'","phonemodel":"'.$phonemodel.'"}]';

        if($result['device_history'] != NULL && $result['device_history'] != "null"){
            $prev_json = (array)json_decode($result['device_history']);
            $next_json = (array)json_decode($device_history,true);
            $array = array_merge($next_json,$prev_json);
            $device_history = json_encode($array);
        }

        $step_2_update = $db->prepare("UPDATE accounts SET imei=?,device_history=?,deviceid=?, appversioncode=?, migration_count=?, country=?, phonemake=?, phonemodel=?, androidversion=?,last_activity_date=? WHERE email=?");
        $step_2_update->bind_param('sssiissssss',$imei,$device_history,$deviceid,$appversioncode,$migration_count,$country,$phonemake,$phonemodel,$androidversion,$now,$email);
        $step_2_update->execute();

        if($step_2_update->errno){
            $msg = "Could not migrate account to new device.";
            send_error($msg);
        }else{
            $step_2_reselect = $db->prepare("SELECT * FROM accounts WHERE email = ?");
            $step_2_reselect->bind_param('s',$email);
            $step_2_reselect->execute();

            $results = $step_2_reselect->get_result();
            $result = $results->fetch_assoc();

            $msg = "Account successfully migrated.";
            send_user($result, $settings, $msg);

            $step_2_reselect->close();
        }

        $step_2_update->close();
        $step_2_select->close();

        $db->close();

        exit;
    }

    $step_2_select->close();


    //Step 3 - Ping the old database and find out if the user is there
    $url = "http://54.201.109.64/server/index.php?r=admeUser/migrate&pw=xftz00215&email=".$email;
    $migrated = json_decode(file_get_contents($url), true);


    if($migrated['status'] == 1){
        //echo "response received successfully";

        $authcode = generateAuthCode();
        $device_history = '[{"imei":"'.$imei.'","date":"'.$today->format('F j, Y').'","phonemake":"'.$phonemake.'","phonemodel":"'.$phonemodel.'"}]';
        $step_3_insert = $db->prepare("INSERT INTO accounts(email, imei, old_user_id, device_history,paypal, country,
										appmode, points_now, points_today, points_year, points_lifetime, points_lastearned,
										deviceid, appversioncode,
										phonemake, phonemodel, androidversion,
										location_enabled, phoneaccess_enabled, last_ip,
										start_date, last_activity_date, days_active,
										num_cashouts, cashout_pending, cashout_amount, cashout_method, redemption_history,
										num_unlocks, last_unlock_time, num_ints_viewed,num_nats_viewed,
										referredby, referrercode, referralcount, referred_users,
										banned, auth_code) 
										VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $step_3_insert->bind_param('ssisssiddddssisssiisssiiidssisiissisii',$email, $imei, $migrated['old_user_id'],$device_history,$migrated['paypal'],$country,
            $migrated['admemode'],$migrated['points_now'],$migrated['points_today'],$migrated['points_year'],$migrated['points_lifetime'], $migrated['points_lastearned'],
            $deviceid, $appversioncode,
            $phonemake, $phonemodel,$androidversion,
            $locationenabled,$phoneaccessenabled, $ip,
            $migrated['start_date'],$migrated['last_activity_date'],$migrated['days_active'],
            $migrated['num_cashouts'],$migrated['cashout_pending'],$migrated['cashout_amount'],$migrated['cashout_method'],json_encode($migrated['redemption_history']),
            $migrated['num_unlocks'],$migrated['last_unlock_time'],$migrated['num_unlocks'],$migrated['num_unlocks'],
            $migrated['referredby'],$migrated['referrercode'], $migrated['referralcount'], json_encode($migrated['referred_users']),
            $migrated['banned'],$authcode);

        $step_3_insert->execute();

        if($step_3_insert->errno){

          //  echo $step_3_insert->error;
            $msg = "Unable to migrate account.";
            send_error($msg);

        }else{

            $step_3_select = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ?");
            $step_3_select->bind_param('ss',$email,$imei);
            $step_3_select->execute();

            $results = $step_3_select->get_result();

            $success = false;
            if($results->num_rows > 0){
                $result = $results->fetch_assoc();

                $msg = "Migrated to Adme v2.";
                send_user($result, $settings, $msg,false);

                $success = true;
            }

            $step_3_select->close();
        }

        $step_3_insert->close();

        if($success){
            $db->close();
            exit;
        }
    }


    //Step 4 - Register a new account and send back account info

    $referrercode = generateReferralCode();

    $authcode = generateAuthCode();

    $starting_bonus = $settings['starting_bonus'];

    $device_history = '[{"imei":"'.$imei.'","date":"'.$today->format('F j, Y').'","phonemake":"'.$phonemake.'","phonemodel":"'.$phonemodel.'"}]';

    $step_4_insert = $db->prepare("INSERT INTO accounts(email, imei, country, device_history,
										points_now, points_today, points_year, points_lifetime,
										deviceid, appversioncode,
										phonemake, phonemodel, androidversion,
										location_enabled, phoneaccess_enabled, last_ip,
										start_date, last_activity_date,
										referredby, referrercode, auth_code) 
										VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $step_4_insert->bind_param('ssssddddsisssiisssssi',$email,$imei,$country,$device_history,
        $starting_bonus,$starting_bonus,$starting_bonus,$starting_bonus,
        $deviceid, $appversioncode,
        $phonemake, $phonemodel,$androidversion,
        $locationenabled,$phoneaccessenabled, $ip,
        $now,$now,
        $referredby,$referrercode,
        $authcode);

    $step_4_insert->execute();

    if($step_4_insert->errno){
       // echo $step_4_insert->error;
        $msg = "Unable to make a new account. There is likely another Adme account currently associated with this device.";
        //$msg = $step_4_insert->error;
        // imei is a unique identifier so if the same imei is attempted to be
        // registered twice it will be rejected.
        send_error($msg);
    }else{
        $step_4_select = $db->prepare("SELECT * FROM accounts WHERE email = ?");
        $step_4_select->bind_param('s',$email);
        $step_4_select->execute();

        $results = $step_4_select->get_result();
        $result = $results->fetch_assoc();

        $msg = "Successfully made new account.";
        send_user($result, $settings, $msg,true);

        $step_4_select->close();
    }

    $step_4_insert->close();

    $db->close();

    exit;

}else{
    $msg = "Information missing. Something is wrong with your device.";
    send_error($msg);
}

function send_error($msg = "") {
    $response["success"] = 0;
    $response["message"] = $msg;

    echo json_encode($response);
}

/**
 * Generate random Alpha numeric code, random.
 */
function generateReferralCode() {
    $length = 6;
    $chars = array_merge ( range ( 0, 9 ), range ( 'A', 'Z' ) );
    shuffle ( $chars );
    $password = implode ( array_slice ( $chars, 0, $length ) );
    return $password;
}

function generateAuthCode() {
    $length = 16;
    $chars = array_merge ( range ( 0, 9 ) );
    shuffle ( $chars );
    $code = implode ( array_slice ( $chars, 0, $length ) );
    return $code;
}

function send_user($result, $settings, $msg = "",$newUser=false) {
    $date = new DateTime('now');
    $date->setTimezone(new DateTimeZone('America/New_York'));
    $str_server_today = $date->format('mYmdYd');
    $today = intval($str_server_today);

    $user;
    $user['email'] = $result['email'];
    $user['paypal'] = $result['paypal'];
    $user['phone_number'] = $result['phone_number'];
    $user['appmode'] = $result['appmode'];
    $user['points_now'] = $result['points_now'];
    $user['cashout_pending'] = $result['cashout_pending'];
    $user['cashout_amount'] = $result['cashout_amount'];
    $user['cashout_method'] = $result['cashout_method'];
    $user['rewards_history']= $result['rewards_history'];
    $user['redemption_history']= $result['redemption_history'];
    $user['referredby'] = $result['referredby'];
    $user['referrercode'] = $result['referrercode'];
    $user['referred_users'] = $result['referred_users'];
    $user['referralcount'] = $result['referralcount'];
    $user['banned'] = $result['banned'];
    $user['auth_code'] = $result['auth_code'];
    $user['sc_points_balance'] = $result['sc_points_balance'];


    $user['min_version'] = $settings['min_version'];
    $user['current_version'] = $settings['current_version'];
    $user['cashout_points_required'] = $settings['cashout_points'];
    $user['referrer_rate'] = $settings['referrer_rate'];
    $user['referred_rate'] = $settings['referred_rate'];
    $user['sc_exchange_rate'] = $settings['sc_exchange_rate'];


    $response["user"] = array();
    array_push($response["user"], $user);


    $response["success"] = 1;
    $response["message"] = $msg;

    if($newUser){
        $response["isnew"]=1;
    }else{
        $response["isnew"]=0;
    }

    echo json_encode($response);
}

?>