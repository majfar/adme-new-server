<?php

// array for JSON response
$response = array();

$email;
$imei;

if (isset($_REQUEST['email']) && isset($_REQUEST['imei']) && isset($_REQUEST['auth']) && isset($_REQUEST['country']) &&  isset($_REQUEST['newmode'])) {

    date_default_timezone_set('America/New_York');

    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_error($msg);
        exit;
    }

    $auth_code = htmlentities(trim($_REQUEST['auth']));
    $imei = htmlentities(trim($_REQUEST['imei']));
    $newmode = htmlentities(trim($_REQUEST['newmode']));

    $country = htmlentities(trim($_REQUEST['country']));

    $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
    $settings_select->bind_param('s',$country);
    $settings_select->execute();

    if($settings_select->errno){
        $msg = "Country information unavailable. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $results = $settings_select->get_result();

    if($results->num_rows > 0){
        $settings = $results->fetch_assoc();
    }else{
        $msg = "Invalid country. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $select_account = $db->prepare("SELECT * FROM accounts WHERE auth_code=? AND email=? AND imei=?");
    $select_account->bind_param('sss',$auth_code,$email,$imei);
    $select_account->execute();

    $results = $select_account->get_result();
    $select_account->close();

    $account = "";

    if($results->num_rows == 0){ //account doesn't exist
        send_error("Unable to find account");
        exit;
    }else {
        $account = $results->fetch_assoc();

    }

    $account['appmode'] = $newmode;


    updateAccount($db,$account);

    sendResponse($newmode);

} else{
    $msg = "Information incomplete. hello";
    send_error($msg);
}

function updateAccount($db, $account){
    $now = new DateTime('now');
    $now = date('Y-m-d G:i:s');
    $update_account = $db->prepare("UPDATE accounts SET 
									appmode=?
									WHERE id=?");
    $update_account->bind_param('ii',
        $account['appmode'],
        $account['id']);
    $update_account->execute();

    if($update_account->errno){
        // echo $update_account->error;
        $msg = "Couldn't save changes";
        send_error($msg);

        $update_account->close();
        exit;
    }

    $update_account->close();
}

function send_error($msg = "") {
    $response["success"] = 0;
    $response["message"] = $msg;

    echo json_encode($response);
}

function sendResponse($mode = 0){
    $response["success"] = 1;
    $response["message"] = "Go";
    $response["mode"] = $mode;

    echo json_encode(utf8_converter($response));
}

function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
            $item = utf8_encode($item);
        }
    });

    return $array;
}

function floor_dec($number,$precision = 3,$separator = '.') {
    $numberpart=explode($separator,$number);
    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
    if($numberpart[0]>=0) {
        $numberpart[1]=substr(floor('1'.$numberpart[1]),1);
    } else {
        $numberpart[1]=substr(ceil('1'.$numberpart[1]),1);
    }
    $ceil_number= array($numberpart[0],$numberpart[1]);
    return implode($separator,$ceil_number);
}

?>