<?php
 
// array for JSON response
$response = array();

$email;
$imei;

if (isset($_REQUEST['email']) && $_REQUEST['imei'] && $_REQUEST['paypal'] && $_REQUEST['auth_code']) {
	
	date_default_timezone_set('America/New_York');
	
	$db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
	if ($db->connect_errno) {
		echo "Sorry, this website is experiencing problems.";
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $mysqli->connect_errno . "\n";
		echo "Error: " . $mysqli->connect_error . "\n";
		exit;
	}
	
	$email = strtolower(htmlentities(trim($_REQUEST['email'])));
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
		$msg = "Account is not valid.";
		send_error($msg);
		exit;
	 }
	
	$imei = htmlentities(trim($_REQUEST['imei']));
	
	$date = new DateTime('now');
	$date->setTimezone(new DateTimeZone('America/New_York'));
	$str_server_today = $date->format('YmdYdm');
	$today = intval($str_server_today);

	$paypal = htmlentities(trim($_REQUEST['paypal']));
    $auth_code = htmlentities(trim($_REQUEST['auth_code']));
	
	if (filter_var($paypal, FILTER_VALIDATE_EMAIL) == false) {
		$msg = "PayPal email is not valid.";
		send_error($msg);
		exit;
	}
	
	$select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ? AND auth_code=?");
	$select_account->bind_param('ssi',$email,$imei,$auth_code);
	$select_account->execute();
	
	$results = $select_account->get_result();
	
	if($results->num_rows == 0){
		$msg = "Account not found.";
		send_error($msg);
		
		$select_account->close();
		exit;
	}
	
	$account = $results->fetch_assoc();
	
	if(($account['num_cashouts'] > 0) && ($account['paypal'] != "")){
		$msg = "PayPal email can't be changed after you've requested rewards.";
		send_error($msg);
		$select_account->close();
		
		exit;
	}
	
	$update_account = $db->prepare("UPDATE accounts SET paypal=?
								WHERE email=? AND imei=? AND auth_code=?");
	$update_account->bind_param('sssi',$paypal,$email,$imei,$auth_code);
	$update_account->execute();

	if($update_account->errno){
		$msg = "Couldn't save changes";
		send_error($msg);
		
		$update_account->close();
		$select_account->close();
		exit;
	}

	$update_account->close();	
	$select_account->close();
	
	send_success();
}else{
    $msg = "Internal Error.";
    send_error($msg);
    exit;
}

function send_error($msg = "") {
        $response["message"] = $msg;
		
		echo json_encode($response);		
    }
	
function send_success() {
	$response["message"] = 1;
	
	echo json_encode($response);		
}
?>