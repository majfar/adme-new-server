<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
// array for JSON response
$response = array();

$email;
$imei;

if (isset($_REQUEST ['email']) && isset($_REQUEST['imei']) && isset($_REQUEST ['country']) && isset ($_REQUEST['method']) && isset ($_REQUEST['auth_code'])) {
	
	date_default_timezone_set('America/New_York');
	
	$db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
	if ($db->connect_errno) {
		echo "Sorry, this website is experiencing problems.";
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $mysqli->connect_errno . "\n";
		echo "Error: " . $mysqli->connect_error . "\n";
		exit;
	}
	
	$email = strtolower(htmlentities(trim($_REQUEST['email'])));
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
		$msg = "Account is not valid.";
		send_error($msg);
		exit;
	 }
	
	$imei = htmlentities(trim($_REQUEST['imei']));
	
	$date = new DateTime('now');
	$date->setTimezone(new DateTimeZone('America/New_York'));
	$str_server_today = $date->format('dmdmYdm');
	$today = intval($str_server_today);
	
	$method = htmlentities(trim($_REQUEST['method']));
	if($method != "paypal" && $method != "card"){
		$msg = "Method is not valid.";
		send_error($msg);
		exit;
	}
	
	$country = htmlentities(trim($_REQUEST['country']));
	
	$settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
	$settings_select->bind_param('s',$country);
	$settings_select->execute();

    $auth_code = htmlentities(trim($_REQUEST['auth_code']));
	
	if($settings_select->errno){
		$msg = "Country information unavailable. Enable Location.";
		send_error($msg);
		
		$settings_select->close();
		exit;
	}
	
	$results = $settings_select->get_result();
	
	if($results->num_rows > 0){
		$settings = $results->fetch_assoc();
	}else{
		$msg = "Adme is not available in your country";
		send_error($msg);
		
		$settings_select->close();
		exit;
	}
	
	$settings_select->close();
	
	$select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ? AND auth_code=?");
	$select_account->bind_param('ssi',$email,$imei,$auth_code);
	$select_account->execute();
	
	$results = $select_account->get_result();
	
	if($results->num_rows == 0){
		$msg = "Account not found.";
		send_error($msg);
		
		$select_account->close();
		exit;
	}
	
	$account = $results->fetch_assoc();
	
	if($account['cashout_pending'] == 1){
		$msg = "A previous redemption is already pending.";
		send_error($msg);
		$select_account->close();
		
		exit;
	}
	
	if($method == "paypal" && ($account['paypal'] == "" || $account['paypal'] == NULL)){
		$msg = "Your PayPal email is invalid.";
		send_error($msg);
		$select_account->close();
		
		exit;
	}
	
	if($account['points_now'] < $settings['cashout_points']){
		$msg = "You do not have enough points to request redemption.";
		send_error($msg);
		$select_account->close();
		
		exit;
	}

    if($account['days_active'] < $settings['min_days_active_cashout']){
        $msg = "You must be active for atleast ".$settings['min_days_active_cashout']." days before you can redeem your rewards.";
        send_error($msg);
        $select_account->close();

        exit;
    }
	
	$cashout_amount = floor_dec($account['points_now'],2); //formats to 2 decimal places
	$remaining_amount = $account['points_now'] - $cashout_amount;
	
	$account['cashout_pending'] = 1;
	$account['num_cashouts'] =+ 1;
	
	$account['points_now'] = $remaining_amount;
	
	$now = new DateTime('now');
	$now = date('Y-m-d G:i:s');
	
	/* removed from here because this belongs during actual payout
	
	$redemption_history = '[{"date":"'.$now->format('F j, Y').'","amount":"$'.$cashout_amount.'"}]';
			
		if($account['redemption_history'] != NULL && $account['redemption_history'] != "null"){
			$prev_json = (array)json_decode($account['redemption_history']);
			$next_json = (array)json_decode($redemption_history,true);
			$array = array_merge($next_json,$prev_json);
			$redemption_history = json_encode($array);
		}*/

	$giveBonus = false;
	if($account['referredby']!=NULL && $account['referrer_bonus_distributed'] == 0){ //first time redeeming and the person who referred them hasn't been given their bonus
	    $accoount['referrer_bonus_distributed'] = 1;
	    $giveBonus = true;
    }

    $rightnow = new DateTime('now');
    $rightnow->setTimezone(new DateTimeZone('America/New_York'));

    $redemption_history = '[{"redemption_date":"'.$rightnow->format('F j, Y').'","amount":"$'.$cashout_amount.'"}]';

    if($account['redemption_history'] != NULL && $account['redemption_history'] != "null"){
        $prev_json = (array)json_decode($account['redemption_history']);
        $next_json = (array)json_decode($redemption_history,true);
        $array = array_merge($next_json,$prev_json);
        $redemption_history = json_encode($array);
    }
	
	$update_account = $db->prepare("UPDATE accounts SET cashout_pending=?, num_cashouts=?, cashout_amount=?,
								points_now=?, last_activity_date=?,cashout_method=?, referrer_bonus_distributed=?,redemption_history=?
								WHERE email=? AND imei=? AND auth_code=?");
	$update_account->bind_param('iidississsi',$account['cashout_pending'],$account['num_cashouts'],$cashout_amount,
										$account['points_now'],$now,$method,$account['referrer_bonus_distributed'],$redemption_history,
                                        $email,$imei,$auth_code);
	$updated = $update_account->execute();

	
	if($update_account->errno || !$updated){
		$msg = "An error occurred (UNSA). Email us at team@adme.mobi"; 
		send_error($msg);
		
		$update_account->close();	
		exit;
	}
	
	$update_account->close();	
	
	$email = "";
	switch($method){
		case "paypal":
		$email = $account['paypal'];
		break;
		case "card":
		$email = $account['email'];
		break;
	}
	
	$insert_statement = $db->prepare("INSERT INTO cashouts(account_id, redemption_email,method,country, amount, request_date) 
										VALUES(?,?,?,?,?,?)");
	$insert_statement->bind_param('isssds',$account['id'], $email,$method,$country, $cashout_amount, $now);
	
	$insert_statement->execute();
	
	if($insert_statement->errno){
		$msg = "An error occurred (UNIA). Email us at team@adme.mobi"; 
		send_error($msg);
		echo $insert_statement->error;
		
		$insert_statement->close();
		exit;
	}
	
	$insert_statement->close();
	
	send_success($cashout_amount,$remaining_amount);

	if($giveBonus){
        $now_unformatted = new DateTime('now');
        $now_unformatted->setTimezone(new DateTimeZone('America/New_York'));
        $now = date('Y-m-d G:i:s');

        $select_referrer_account = $db->prepare("SELECT * FROM accounts WHERE referrercode = ?");
        $select_referrer_account->bind_param('s',$account['referredby']);
        $select_referrer_account->execute();

        $results = $select_referrer_account->get_result();

        $bonus;
        if($results->num_rows > 0) { //was referred by another user
            $referrer_account = $results->fetch_assoc();
            $select_referrer_account->close();

            if ($referrer_account['banned'] == 1) {
                exit;
            }

            $bonus = $settings['referred_rate'];

            $referrer_account['referralcount'] += 1;
            $referrer_account['points_today_bonus'] += $settings['referrer_rate'];

            $email = obfuscate_email($account['email']);

            $referred_users = '[{"email":"' . $email . '","signup_date":"' . $now_unformatted->format('F j, Y') . '"}]';

            if ($referrer_account['referred_users'] != NULL && $referrer_account['referred_users'] != "null") {
                $prev_json = (array)json_decode($referrer_account['referred_users']);
                $next_json = (array)json_decode($referred_users, true);
                $array = array_merge($next_json, $prev_json);
                $referred_users = json_encode($array);
            }

            $update_referrer = $db->prepare("UPDATE accounts SET referralcount=?, points_today_bonus=?, referred_users=?,last_activity_date=?
								WHERE id=?");
            $update_referrer->bind_param('idssi', $referrer_account['referralcount'], $referrer_account['points_today_bonus'], $referred_users,
                $now, $referrer_account['id']);
            $update_referrer->execute();

            $update_referrer->close();
        }
    }
}else{
	send_error("attributes missing");
}

function send_error($msg = "") {
		$response["success"] = 0;
        $response["message"] = $msg;
		
		echo json_encode($response);		
    }
	
function send_success($cashout_amount,$remaining_amount) {
	$response["success"] = 1;
	$response["amount"] = $cashout_amount;
	$response["balance"] = $remaining_amount;
	$response["message"] = "Rewards requested successfully";
	
	echo json_encode($response);		
}

function floor_dec($number,$precision = 2,$separator = '.') {
  $numberpart=explode($separator,$number);
  $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
  if($numberpart[0]>=0) {
    $numberpart[1]=substr(floor('1'.$numberpart[1]),1);
  } else {
    $numberpart[1]=substr(ceil('1'.$numberpart[1]),1);
  }
  $ceil_number= array($numberpart[0],$numberpart[1]);
  return implode($separator,$ceil_number);
}
?>