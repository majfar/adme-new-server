<?php

if(isset($_GET['file'])){
	$filename = $_GET['file'];
}else{
	echo "No filename provided.";
	exit;
}

$files = array("masspay_paypal_".$filename, "fraud_paypal_".$filename,"masspay_card_".$filename, "fraud_card_".$filename);
$zipname = $filename.".zip";
$zip = new ZipArchive;
$zip->open($zipname, ZipArchive::CREATE);
foreach ($files as $file) {
  //$zip->addFile($file);
  $content = file_get_contents($file);
$zip->addFromString(pathinfo ( $file, PATHINFO_BASENAME), $content);
}
$zip->close();

header('Content-Type: application/zip');
header('Content-disposition: attachment; filename='.$zipname);
header('Content-Length: ' . filesize($zipname));
readfile($zipname);

?>