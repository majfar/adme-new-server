<?php

date_default_timezone_set('America/New_York');

$db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
if ($db->connect_errno) {
    echo "Sorry, this website is experiencing problems.";
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}

require 'fraud_functions.php';


$now = new DateTime('now');
$now = date('Y-m-d');
$select_active_accounts = $db->prepare("SELECT * FROM accounts WHERE last_activity_date > ?");
$select_active_accounts->bind_param('s',$now);
$select_active_accounts->execute();

$results = $select_active_accounts->get_result();
$select_active_accounts->close();

$active_count = $results->num_rows;

$flagged_users = array();
$unlocks = 0;
$points_distributed = 0;
$active_time = 0; // used to calculate the # of days that a user stays.
$ad_views = 0;
$bonus_points = 0;
$bonus_users = 0;
$ls_active = 0;
$ls_disabled = 0;
while ($account = $results->fetch_assoc()) {
    $unlocks += $account['num_unlocks_today'];
    if($account['points_lastearned'] > $now){
        $points_distributed += $account['points_today'];
    }
    if($account['bonus_lastvisited'] > $now){
        $bonus_points += $account['points_today_bonus'];
        $bonus_users += 1;
    }
    $active_time += $account['days_active'];
    $reason = "";
    if($account['migration_count'] > 3){
        $reason .= "Too many migrations (".$account['migration_count']."), ";
    }

    /* if($account['quality_score'] <= 40){
        $reason .= "Low quality score (".$account['quality_score']."), ";
    } */

    if($account['num_unlocks_today'] > 250){
        $reason .= "High number of unlocks (".$account['num_unlocks_today']."), ";
    }

    if($account['num_cashouts'] > 8){
        $reason .= "High number of cashouts (".$account['num_cashouts']."), ";
    }

    if($account['ls_enabled'] == 1){
        $ls_active += 1;
    }else{
        $ls_disabled += 1;
    }

    if($account['referralcount'] >= 10){
        $referral_scores = check_referrals($account, $db);
        if($referral_scores == NULL){
            $reason .= "Couldn't find the referrals that were claimed, ";
        }else{
            if($account['referralcount'] > $referral_scores['count']){
                $reason .= "User credited for more users than referred (".$account['referralcount'].",".$referral_scores['count']."), ";
            }
            /* if($referral_scores['active_days'] < 5){
                $reason .= "Refferals (".$referral_scores['count'].") are not active very long (avg "
                            .$referral_scores['active_days']." days), ";
            }
            if($referral_scores['zero_actives'] >= 20){
                $reason .= $referral_scores['zero_actives']."% of ".$referral_scores['count']." referrals have never been active, ";
            }
            if($referral_scores['low_unlocks'] >= 20){
                $reason .= $referral_scores['low_unlocks']."% of ".$referral_scores['count']." referrals have less than 40 unlocks, ";
            }
            if($referral_scores['low_quality_score'] >= 20){
                $reason .= $referral_scores['low_quality_score']."% of ".$referral_scores['count']." referrals have less than 50 quality score, ";
            }
            if($referral_scores['low_points'] >= 20){
                $reason .= $referral_scores['low_points']."% of ".$referral_scores['count']." referrals have less than 20 points, ";
            } */

        }
    }

    if($account['points_today'] > 500){
        $reason .= "Too many points today (".$account['points_today']."), ";
    }

    if($account['paypal'] != NULL){
        $paypal_duplicate_count = check_paypal_usage($account['paypal'],$db);
        if($paypal_duplicate_count > 2){
            $reason .= "PayPal account used multiple times (".$paypal_duplicate_count."), ";
        }
    }

    if($reason != ""){
        $flagged = array();
        $flagged['id'] = $account['id'];
        $flagged['reason'] = $reason;
        array_push($flagged_users,$flagged);
    }
}

$upu = round($unlocks / $active_count,1);
$ppu = round($points_distributed / $active_count,4);
$aat = round($active_time / $active_count,1);
$ppul = round($points_distributed / $unlocks,6);
$ppma = $points_distributed / ($ad_views / 1000);

$to = "fareed@adme.mobi,majeed@adme.mobi";

if(count($flagged_users) > 0){
    $subject = "Fraud Alert: ".count($flagged_users)." user(s)";
}else{
    $subject = $active_count." users, ".$unlocks." unlocks(".$upu." u/u), ".$points_distributed." points(".$ppu." p/u)";
}
$message = "Daily summary for ".$now.
    "\n"."Active Users Today: ".$active_count.
    "\n"."Unlocks Today: ".$unlocks.
    "\n"."Unlocks Per User: ".$upu.
    "\n"."Unlock Points Distributed Today: ".$points_distributed.
    "\n"."Bonus Points Distributed Today: ".$bonus_points.
    "\n"."Bonus Users: ".$bonus_users.
    "\n"."Points Per User : ".$ppu.
    "\n"."Points Per Unlock : ".$ppul.
    "\n"."Bonus Points Distributed Today: ".$bonus_points.
    "\n"."Users Participating in Bonuses: ".$bonus_users.
    "\n"."Average Days Active : ".$aat.
    "\n".
    "\n"."Fraud Findings For Today's Active Users: ".
    "\n"
;
foreach($flagged_users as $fraudster){
    $message .= "User ID: ".$fraudster['id']."    Reason: ".$fraudster['reason']."\n";
}
$from = "Adme Daily Report <server@adme.mobi>";
$headers = "From:" . $from;
if(mail($to,$subject,$message,$headers)){
    echo "Mail Sent.";
}else{
    $errorMessage = error_get_last()['message'];
    echo "Mail Failed. ".$errorMessage;
}

function floor_dec($number,$precision = 2,$separator = '.') {
    $numberpart=explode($separator,$number);
    $numberpart[1]=substr_replace($numberpart[1],$separator,$precision,0);
    if($numberpart[0]>=0) {
        $numberpart[1]=substr(floor('1'.$numberpart[1]),1);
    } else {
        $numberpart[1]=substr(ceil('1'.$numberpart[1]),1);
    }
    $ceil_number= array($numberpart[0],$numberpart[1]);
    return implode($separator,$ceil_number);
}

?>

