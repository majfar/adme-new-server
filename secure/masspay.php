<?php
//echo exec('whoami');

error_reporting(E_ALL);
ini_set('display_errors', 1);

/* session_start();
if (!isset($_SESSION['auth']) || $_SESSION['auth'] != 1) {
   header('Location: login.php');
   exit();
} */

date_default_timezone_set('America/New_York');

$db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
if ($db->connect_errno) {
    echo "Sorry, this website is experiencing problems.";
    echo "Error: Failed to make a MySQL connection, here is why: \n";
    echo "Errno: " . $mysqli->connect_errno . "\n";
    echo "Error: " . $mysqli->connect_error . "\n";
    exit;
}

require 'fraud_functions.php';
$now = new DateTime('now');
$now->setTimezone(new DateTimeZone('America/New_York'));
$now_formatted = $now->format('Y-m-d G:i:s');

// fetch the data
$method = "paypal";
$select_cashouts_paypal = $db->prepare("SELECT * FROM cashouts WHERE added_to_file=0 AND method=?");
$select_cashouts_paypal->bind_param('s',$method);
$select_cashouts_paypal->execute();

$results = $select_cashouts_paypal->get_result();
$select_cashouts_paypal->close();
/* if($results->num_rows == 0){
	echo "No cashouts? Seems weird.";
	exit;
} */


$update_cashouts_paypal = $db->prepare("UPDATE cashouts SET added_to_file=1, pay_date=?
								WHERE added_to_file=0 AND method=?");
$update_cashouts_paypal->bind_param('ss',$now_formatted,$method);
$update_cashouts_paypal->execute();

if($update_cashouts_paypal->errno){
    echo "Something is going wrong while updating.";

    $update_cashouts_paypal->close();
    exit;
}

$update_cashouts_paypal->close();

// output headers so that the file is downloaded rather than displayed 
// uncomment to allow downloading immediately
/* header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=/secure/mass_pay_files/data.csv'); */
$directory = '/var/www/html/secure/mass_pay_files/';
//$directory = "";
$masspay_filename = 'masspay_paypal_';
$fraud_filename = 'fraud_paypal_';
$csv_filename = date("Y-m-d_H-i",time()).'.csv';

// create a file pointer connected to the output stream
//$output = fopen('php://output', 'w'); //uncomment to allow for immediate downloading
//$output = fopen($csv_filename, 'w');

// loop over the rows, outputting them
$masspay_data = "";
$fraud_data = "User ID, User Email, Cashout Amount, Country Code, Days Active, Total Unlocks, Total Ad Views, "
    ."Unlocks per day, Points per day, "
    ."Points This Year, Points Lifetime, Number of Cashouts, Quality Score, Referral Count (acct tbl), "
    ."Referral Count (counted), Bonus Count, LS Enabled, Banned, App Version Code, Number of Migrations, "
    ."Total Accounts with same Paypal, % of Referrals never active, "
    ."% of Referrals <40 unlocks, % of Referrals Quality Score < 50, % of Referrals < 20 points, "
    ."Avg # of Days active for Referrals \n";
//echo "Loading.";
if($results->num_rows > 0){
    while ($redeemer = $results->fetch_assoc()) {
        $masspay_data .= $redeemer['paypal_email'].",".$redeemer['amount'].",USD\n";
        $fraud_data .= run_full_fraud_check($redeemer['account_id'], $db);
        update_account_table($now,$redeemer['account_id'],$redeemer['amount'],$db);
    }
}

$csv_handler1 = fopen ($directory.$masspay_filename.$csv_filename,'w');
fwrite ($csv_handler1,$masspay_data);
fclose ($csv_handler1);
chgrp($directory.$masspay_filename.$csv_filename, 'ec2-ginx');
chmod($directory.$masspay_filename.$csv_filename, 0777);

$csv_handler2 = fopen ($directory.$fraud_filename.$csv_filename,'w');
fwrite ($csv_handler2,$fraud_data);
fclose ($csv_handler2);
chgrp($directory.$fraud_filename.$csv_filename, 'ec2-ginx');
chmod($directory.$fraud_filename.$csv_filename, 0777);


/*Do it all again for Tango Card*/

// fetch the data
$method = "card";
$select_cashouts_card = $db->prepare("SELECT * FROM cashouts WHERE added_to_file=0 AND method=?");
$select_cashouts_card->bind_param('s',$method);
$select_cashouts_card->execute();


$results = $select_cashouts_card->get_result();
$select_cashouts_card->close();
/* if($results->num_rows == 0){
	echo "No cashouts? Seems weird.";
	exit;
} */

$update_cashouts_card = $db->prepare("UPDATE cashouts SET added_to_file=1, pay_date=?
								WHERE added_to_file=0 AND method=?");
$update_cashouts_card->bind_param('ss',$now_formatted,$method);
$update_cashouts_card->execute();

if($update_cashouts_card->errno) {
    echo "Something is going wrong while updating.";

    $update_cashouts_card->close();
    exit;
}

$update_cashouts_card->close();

// output headers so that the file is downloaded rather than displayed 
// uncomment to allow downloading immediately
/* header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=/secure/mass_pay_files/data.csv'); */
$directory = '/var/www/html/secure/mass_pay_files/';
//$directory = "";
$masspay_filename = 'masspay_card_';
$fraud_filename = 'fraud_card_';
$csv_filename = date("Y-m-d_H-i",time()).'.csv';

// create a file pointer connected to the output stream
//$output = fopen('php://output', 'w'); //uncomment to allow for immediate downloading
//$output = fopen($csv_filename, 'w');

// loop over the rows, outputting them
$masspay_data = "";
$fraud_data = "User ID, User Email, Cashout Amount, Country Code, Days Active, Total Unlocks, Total Ad Views, "
    ."Unlocks per day, Points per day, "
    ."Points This Year, Points Lifetime, Number of Cashouts, Quality Score, Referral Count (acct tbl), "
    ."Referral Count (counted), Bonus Count, LS Enabled, Banned, App Version Code, Number of Migrations, "
    ."Total Accounts with same Paypal, % of Referrals never active, "
    ."% of Referrals <40 unlocks, % of Referrals Quality Score < 50, % of Referrals < 20 points, "
    ."Avg # of Days active for Referrals \n";
//echo "Loading.";
if($results->num_rows > 0){
    while ($redeemer = $results->fetch_assoc()) {
        $masspay_data .= $redeemer['email'].",".$redeemer['amount'].",USD\n";
        $fraud_data .= run_full_fraud_check($redeemer['account_id'], $db);
        update_account_table($now,$redeemer['account_id'],$redeemer['amount'],$db);
    }
}

$csv_handler1 = fopen ($directory.$masspay_filename.$csv_filename,'w');
fwrite ($csv_handler1,$masspay_data);
fclose ($csv_handler1);
chgrp($directory.$masspay_filename.$csv_filename, 'ec2-ginx');
chmod($directory.$masspay_filename.$csv_filename, 0777);

$csv_handler2 = fopen ($directory.$fraud_filename.$csv_filename,'w');
fwrite ($csv_handler2,$fraud_data);
fclose ($csv_handler2);
chgrp($directory.$fraud_filename.$csv_filename, 'ec2-ginx');
chmod($directory.$fraud_filename.$csv_filename, 0777);


//chgrp($csv_filename, "ec2-ginx");

//echo "done!";

$to = "fareed@adme.mobi,majeed@adme.mobi";

$subject = "Mass Pay & Fraud files are ready to be downloaded";
$message = "The two files can be downloaded from the following link: \n\n"
    ."https://server.adme.mobi/secure/mass_pay_files/download.php?file=";
$message .= htmlspecialchars($csv_filename,ENT_QUOTES);

$from = "Adme Payday <server@adme.mobi>";
$headers = "From:" . $from;
mail($to,$subject,$message,$headers);

function update_account_table($now,$id,$amount, $db){
$update_account = $db->prepare("UPDATE accounts SET cashout_pending=0, cashout_amount=0
							WHERE id=?");
$update_account->bind_param('i',$id);
$update_account->execute();

if($update_account->errno){
    echo "Something is going wrong while updating.";

    $update_account->close();
    exit;
}

$update_account->close();
}
?>