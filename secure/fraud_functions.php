<?php 

//Assumes that the files calling this helper file already have a database connection

function check_referrals($account, $db){
	//todo check average # of active days
	$scores = array();
	$select_accounts = $db->prepare("SELECT * FROM accounts WHERE referredby = ?");
	$select_accounts->bind_param('s',$account['referrercode']);
	$select_accounts->execute();
	
	$results = $select_accounts->get_result();
	
	$select_accounts->close();
	
	if($results->num_rows == 0){
		return NULL;
	}

	$scores['count'] = $results->num_rows;
	
	$accounts = $results->fetch_assoc();
	
	$active_days = 0;
	$zero_actives = 0;
	$unlocks = 0;
	$low_unlocks = 0;
	$low_points = 0;
	foreach ($accounts as $referred_account){
		if($referred_account == null){
			echo $account['id']." has null referral";
		}else{
			$active_days += $referred_account['days_active'];
			if($referred_account['days_active'] == 0){
				$zero_actives += 1;
			}
			$unlocks += $referred_account['num_unlocks'];
			if($referred_account['num_unlocks'] < 40){
				$low_unlocks += 1;
			}
			if($referred_account['current_points'] < 450 && $referred_account['points_today'] < 10){
				$low_points += 1;
			}
		}
	}
	
	$scores['active_days'] = $active_days / $scores['count'];
	$scores['zero_actives'] = $zero_actives / $scores['count'];
	$scores['low_unlocks'] = $low_unlocks / $scores['count'];
	$scores['low_points'] = $low_points / $scores['count'];
	
	return $scores;
}

function check_paypal_usage($email, $db){
	if($email != NULL){
		$select_paypal = $db->prepare("SELECT * FROM accounts WHERE paypal = ?");
		$select_paypal->bind_param('s',$email);
		$select_paypal->execute();
		
		$results = $select_paypal->get_result();
		
		return $results->num_rows;
	}else{
		return 0;
	}
	
}

function run_full_fraud_check($id, $db){
	if($id == NULL){
		return "\n";
	}
	$select_account = $db->prepare("SELECT * FROM accounts WHERE id=?");
	$select_account->bind_param('i',$id);
	$select_account->execute();
	
	$results = $select_account->get_result();
	
	$select_account->close();
	
	if($results->num_rows == 0){
		return $id."\n";
	}
	
	$account = $results->fetch_assoc();
	$referral_scores = check_referrals($account, $db);
	$paypal_usage = check_paypal_usage($account['paypal'],$db);
	
	$upd = $account['num_unlocks'] / $account['days_active'];
	$ppd = $account['points_lifetime'] / $account['days_active'];
	
	$response = $id.",".$account['paypal'].",".$account['cashout_amount'].",".$account['country'].","
			.$account['days_active'].",".$account['num_unlocks'].",".$account['num_adviews'].","
			.$upd.",".$ppd.","
			.$account['points_year'].",".$account['points_lifetime'].",".$account['num_cashouts'].","
			.$account['referralcount'].",".$referral_scores['count'].","
			.$account['ls_enabled'].",".$account['banned'].",".$account['appversioncode'].","
			.$account['migration_count'].",".$paypal_usage.",".$referral_scores['zero_actives'].","
			.$referral_scores['low_unlocks'].",".$referral_scores['low_quality_score'].","
			.$referral_scores['low_points'].",".$referral_scores['active_days']."\n";
			
	return $response;
}


?>