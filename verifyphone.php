<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1);
array for JSON response*/
$response = array();
 
if (isset($_REQUEST ['email']) && isset($_REQUEST['imei']) && isset($_REQUEST ['phone']) && isset($_REQUEST ['auth'])) {

    date_default_timezone_set('America/New_York');


    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_response($msg);
        exit;
    }


    $imei = htmlentities(trim($_REQUEST['imei']));

    if($imei == ""){
        $msg = "Incomplete Device Information. Tablets are not allowed.";
        send_response($msg);

        exit;
    }

    
    $phone = htmlentities(trim($_REQUEST['phone']));
    
	if($phone == "" ){
        $msg = "Invalid phone number "+$phone;
        send_response($msg);

        exit;
    }

    $auth_code = htmlentities(trim($_REQUEST['auth']));
	
	$ip = $_SERVER['REMOTE_ADDR'];
	

	$prepare = $db->prepare("UPDATE accounts SET phone_number = ?, last_ip=? WHERE email=? AND imei=? AND auth_code=?");
	$prepare->bind_param('ssssi',$phone,$ip,$email,$imei,$auth_code);
	if($prepare->execute()){
		send_response("1");
		}else{
			//echo $prepare->error;
			send_response("Error saving phone number");
		}


    $db->close();

}else{
    $msg = "Information missing. Something is wrong with your device.";
    send_response($msg);
}


function send_response($msg = "") {
        $response["message"] = $msg;

        echo json_encode($response);
    }


?>