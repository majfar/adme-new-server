<?php

// array for JSON response
$response = array();

$email;
$imei;

if (isset($_REQUEST['email']) && isset($_REQUEST['imei']) && isset($_REQUEST['country']) && isset($_REQUEST['auth_code']) && isset($_REQUEST['type'])) {

    date_default_timezone_set('America/New_York');

    $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
    if ($db->connect_errno) {
        echo "Sorry, this website is experiencing problems.";
        echo "Error: Failed to make a MySQL connection, here is why: \n";
        echo "Errno: " . $mysqli->connect_errno . "\n";
        echo "Error: " . $mysqli->connect_error . "\n";
        exit;
    }

    $email = strtolower(htmlentities(trim($_REQUEST['email'])));
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
        $msg = "Email is not valid.";
        send_error($msg);
        exit;
    }

    $imei = htmlentities(trim($_REQUEST['imei']));

    $auth_code = htmlentities(trim($_REQUEST['auth_code']));
    $type = htmlentities(trim($_REQUEST['type']));

    switch ($type){
        case "initial":
        case "reset":
        case "continue":
            break;
        default:
            $msg = "Type is not valid. Type: ".$type;
            send_error($msg);
            exit;
    }

    $appversioncode = 0;
    $question = 0;
    $response = "";
    $country = "";
    $questionType = "";
    $ansDetails = "";
    $bonusCode = "";

    $country = htmlentities(trim($_REQUEST['country']));

    if(isset($_REQUEST['appversioncode'])){
        $appversioncode = htmlentities(trim($_REQUEST['appversioncode']));
    }

    if(isset($_REQUEST['question'])){ //the number for the question that just got answered
        $question = htmlentities(trim($_REQUEST['question']));
    }

    if(isset($_REQUEST['qt'])){ //question type. type "p" means profile. sent in parameter "pd"
        $questionType = htmlentities(trim($_REQUEST['qt']));
    }

    if(isset($_REQUEST['ad'])){ //the details related to the answer of the question
        $ansDetails = htmlentities(trim($_REQUEST['ad']));
    }

    if(isset($_REQUEST['response'])){ //letter choice of response
        $response = htmlentities(trim($_REQUEST['response']));
    }

    if(isset($_REQUEST['bonuscode'])){
        $bonusCode = htmlentities(trim($_REQUEST['bonuscode']));
    }

    $settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
    $settings_select->bind_param('s',$country);
    $settings_select->execute();

    if($settings_select->errno){
        $msg = "Country information unavailable. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $results = $settings_select->get_result();

    if($results->num_rows > 0){
        $settings = $results->fetch_assoc();
    }else{
        $msg = "Invalid country. Enable GPS.";
        send_error($msg);

        $settings_select->close();
        exit;
    }

    $select_account = $db->prepare("SELECT * FROM accounts WHERE imei=? AND email=?");
    $select_account->bind_param('ss',$imei,$email);
    $select_account->execute();

    $results = $select_account->get_result();
    $select_account->close();

    $account = "";

    if($results->num_rows == 0){ //account doesn't exist. Make a new one
        send_error("Unable to find account");
        exit;
    }else {
        $account = $results->fetch_assoc();

    }

    $jsonurl = "https://server.surveycow.co/bonus_sdk.php?&id=".$auth_code."&app=adme&question=".$question."&qt=".$qt."&ad=".$ad."&response=".$response."&country=".$country;
    //echo $jsonurl;
    $json = file_get_contents($jsonurl);
    $obj = json_decode($json, true);
    $question = $obj['question'];

    if($type=="initial" || $type == "reset"){
        $account['sc_max_approved'] = $settings['sc_questions_max'];

    }else if ($type == "continue"){
        $account['sc_max_approved'] -= 1;
    }

    if($account['sc_max_approved']<=0){
        updateAccount($db,$account,$now);
        loadAd($question);
        exit;
    }

    $account['sc_bonus_questions_total'] += 1;

    $now = new DateTime('now');
    $date = new DateTime($account['sc_last_bonus_date']);
    $day1 = $date->format('d');
    $day2 = $now->format('d');
    $daysBetween = $day2 - $day1;

    if($daysBetween > 0){
        $account['sc_bonus_questions_today'] = 1;
        $account['sc_bonus_days'] += 1;
    }else{
        $account['sc_bonus_questions_today'] += 1;
    }

    $account['sc_bonus_questions_total'] += 1;


    $account['sc_points_balance'] += $settings['sc_points_per_question'];

    updateAccount($db,$account);

    sendResponse($question,$account['sc_points_balance']);

} else{
    $msg = "Information incomplete. hello";
    send_error($msg);
}

function timeToSeconds($time){
    $timeExpanded = explode(':', $time);
    if (isset($timeExpanded[2])) {
        return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60 + $timeExpanded[2];
    }
    return $timeExpanded[0] * 3600 + $timeExpanded[1] * 60;
}

function updateAccount($db, $account){
    $now = new DateTime('now');
    $now = date('Y-m-d G:i:s');
    $update_account = $db->prepare("UPDATE accounts SET 
									sc_max_approved=?, sc_bonus_questions_total=?, sc_bonus_questions_today=?, 
									sc_last_bonus_date=?, sc_bonus_days=?,sc_points_balance=?
									WHERE id=?");
    $update_account->bind_param('iiisiii',
        $account['sc_max_approved'],$account['sc_bonus_questions_total'],
        $account['sc_bonus_questions_today'],$now,$account['sc_bonus_days'],
        $account['sc_points_balance'],
        $account['id']);
    $update_account->execute();

    if($update_account->errno){
       // echo $update_account->error;
        $msg = "Couldn't save changes";
        send_error($msg);

        $update_account->close();
        exit;
    }

    $update_account->close();
}

function loadAd($question) {
    $response["success"] = 2;
    $response["message"] = "A";
    $response["question"] = $question;

    echo json_encode($response);
}

function send_error($msg = "") {
    $response["success"] = 0;
    $response["message"] = $msg;

    echo json_encode($response);
}

function sendResponse($question, $balance = 0){
    $response["success"] = 1;
    $response["message"] = "Go";
    $response["question"] = $question;
    $response["balance"] = $balance;

    echo json_encode(utf8_converter($response));
}

function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
            $item = utf8_encode($item);
        }
    });

    return $array;
}

?>