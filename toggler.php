<?php
 
// array for JSON response
$response = array();

$email;
$imei;

if (isset($_REQUEST['email']) && isset($_REQUEST['imei']) && isset($_REQUEST['switch']) && isset($_REQUEST['position'])) {
	
	date_default_timezone_set('America/New_York');
	
	$db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
	if ($db->connect_errno) {
		echo "Sorry, this website is experiencing problems.";
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $mysqli->connect_errno . "\n";
		echo "Error: " . $mysqli->connect_error . "\n";
		exit;
	}	
	
	$email = strtolower(htmlentities(trim($_REQUEST['email'])));
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
		$msg = "Email is not valid.";
		send_error($msg);
		exit;
	 }
	 
	 $imei = htmlentities(trim($_REQUEST['imei']));
	 
	 $switch = htmlentities(trim($_REQUEST['switch']));
	 
	 
	 $now = new DateTime('now');
	 $now = date('Y-m-d G:i:s');
	 
	 if($switch == "feedtype"){
		 
	 $position = htmlentities(trim($_REQUEST['position']));
	 
		$update_account = $db->prepare("UPDATE accounts SET contentfeed_priority=?
									WHERE email=? AND imei=?");
		$update_account->bind_param('sss',$position,$email,$imei);
		$update_account->execute();	
		
		if($update_account->errno){
			$response["success"] = 0;
			
			echo json_encode($response);	
			
			$update_account->close();
			exit;
		}
		
		$update_account->close();
		
		$response["success"] = 1;	
		echo json_encode($response);	
		
	}
	 
}
?>