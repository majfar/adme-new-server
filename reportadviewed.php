<?php
 

$email;
$imei;

//phpinfo();

if (isset($_REQUEST ['email']) && isset($_REQUEST['imei']) && isset($_REQUEST['adtype'])) {
	
	date_default_timezone_set('America/New_York');
	
	
	 $db = new mysqli('52.42.237.198', 'root', 'MajFar123$', 'adme') or die("MySQL error: ". mysqli_error());
	if ($db->connect_errno) {
		echo "Sorry, this website is experiencing problems.";
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $mysqli->connect_errno . "\n";
		echo "Error: " . $mysqli->connect_error . "\n";
		exit;
	}	

	$email = strtolower(htmlentities(trim($_REQUEST['email'])));
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
		$msg = "Email is not valid.";
		send_response($msg);
		exit;
	 }
	
	
	$imei = htmlentities(trim($_REQUEST['imei']));
	
	if($imei == ""){
		$msg = "Incomplete Device Information. Tablets are not allowed.";
		send_response($msg);
		
		exit;
	}
	
	$adtype = htmlentities(trim($_REQUEST['adtype']));
	
	if($imei == ""){
		$msg = "Incomplete information provided.";
		send_response($msg);
		
		exit;
	}
	
	
	$ip = $_SERVER['REMOTE_ADDR'];
	
	$now = new DateTime('now');
	$now = date('Y-m-d G:i:s');
	
	if($adtype == "native"){
		$prepare = $db->prepare("UPDATE accounts SET num_nats_viewed = num_nats_viewed + 1, last_ip=? WHERE email=? AND imei=?");
	}else if($adtype == "interstitial"){
		$prepare = $db->prepare("UPDATE accounts SET num_ints_viewed = num_ints_viewed + 1, last_ip=? WHERE email=? AND imei=?");
	}else{
		$msg = "Invalid ad type";
		send_response($msg);
		
		exit;
	}
	
	
	$prepare->bind_param('sss',$ip,$email,$imei);
	$prepare->execute();
	
	$msg = "Done.";
	send_response($msg);
	
	$prepare->close();
	$db->close();

	exit; 
	
}else{
	$msg = "Information missing. Something is wrong with your device.";
	send_response($msg);
}

function send_response($msg = "") {
		$response["success"] = 0;
        $response["message"] = $msg;
		
		echo json_encode($response);		
    }
	

?>