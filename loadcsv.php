<?php

date_default_timezone_set('America/New_York');
	
$db = new mysqli('database.surveycow.co', 'server', 'MajFar123$', 'surveycow') or die("MySQL error: ". mysqli_error());
if ($db->connect_errno) {
	echo "Sorry, this website is experiencing problems.";
	echo "Error: Failed to make a MySQL connection, here is why: \n";
	echo "Errno: " . $mysqli->connect_errno . "\n";
	echo "Error: " . $mysqli->connect_error . "\n";
	exit;
}
	
$csvFile = file('survey.csv');
$data = [];
$lines = 0;

foreach ($csvFile as $line) {
	$lines += 1;
	$data = explode(";", $line);
	
	$question = $data[0];
	$opt_a = $data[1];
	$opt_b = $data[2];
	$opt_c = $data[3];
	$opt_d = $data[4];
	$correct_ans = $data[5]; 
	$quality = $data[6];
	$profile = $data[7];
	$category = $data[8];
	
	if($quality == 1){
		$insert_statement = $db->prepare("INSERT INTO questions(question, option_a,option_b, option_c, option_d,
										correct_answer, quality_check) 
										VALUES(?,?,?,?,?,?,?)");
		$insert_statement->bind_param('ssssssi',$question ,$opt_a,$opt_b, $opt_c, $opt_d,$correct_ans,$quality);
	}else if($profile == 1){
		$insert_statement = $db->prepare("INSERT INTO questions(question, option_a,option_b, option_c, option_d,
										personality, personality_category) 
										VALUES(?,?,?,?,?,?,?)");
		$insert_statement->bind_param('sssssis',$question ,$opt_a,$opt_b, $opt_c, $opt_d,$profile,$category);
	}else{
		$insert_statement = $db->prepare("INSERT INTO questions(question, option_a,option_b, option_c, option_d) 
										VALUES(?,?,?,?,?)");
		$insert_statement->bind_param('sssss',$question ,$opt_a,$opt_b, $opt_c, $opt_d);
	}
	
	$insert_statement->execute();
	
	if($insert_statement->errno){
		echo "Error inserting line ".$lines.". Error: ".$db->error."\n";
		echo var_dump($opt_a);
		exit;
	}
	
	$insert_statement->close();
}

echo "Added ".$lines." lines.";

?>