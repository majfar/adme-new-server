<?php
 
// array for JSON response
$response = array();

$email;
$imei;

if (isset($_GET['email']) && isset($_GET['imei']) && isset($_GET['country']) && isset($_GET['auth_code'])) {
	
	date_default_timezone_set('America/New_York');
	
	$db = new mysqli('database.surveycow.co', 'server', 'MajFar123$', 'surveycow') or die("MySQL error: ". mysqli_error());
	if ($db->connect_errno) {
		echo "Sorry, this website is experiencing problems.";
		echo "Error: Failed to make a MySQL connection, here is why: \n";
		echo "Errno: " . $db->connect_errno . "\n";
		echo "Error: " . $db->connect_error . "\n";
		exit;
	}
	
	$email = strtolower(htmlentities(trim($_GET['email'])));
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
		$msg = "Account is not valid.";
		send_error($msg);
		exit;
	 }
	
	$imei = htmlentities(trim($_GET['imei']));
	
	$date = new DateTime('now');
	 $date->setTimezone(new DateTimeZone('America/New_York'));
	 $str_server_today = $date->format('YmdYdm');
	 $today = intval($str_server_today);
	
	 $auth_code = intval(htmlentities(trim($_GET['auth_code']))) - $today;
	
	$country = htmlentities(trim($_GET['country']));
	
	$settings_select = $db->prepare("SELECT * FROM settings WHERE country_code = ?");
	$settings_select->bind_param('s',$country);
	$settings_select->execute();
	
	if($settings_select->errno){
		$msg = "Country information unavailable. Enable GPS.";
		send_error($msg);
		
		$settings_select->close();
		exit;
	}
	
	$results = $settings_select->get_result();
	
	if($results->num_rows > 0){
		$settings = $results->fetch_assoc();
	}else{
		$msg = "Invalid country. Enable GPS.";
		send_error($msg);
		
		$settings_select->close();
		exit;
	}
	
	$settings_select->close();
	
	$select_account = $db->prepare("SELECT * FROM accounts WHERE email = ? AND imei = ? AND auth_code = ?");
	$select_account->bind_param('ssi',$email,$imei,$auth_code);
	$select_account->execute();
	
	$results = $select_account->get_result();
	
	if($results->num_rows == 0){
		$msg = "Account not found.";
		send_error($msg);
		
		$select_account->close();
		exit;
	}
	
	$account = $results->fetch_assoc();
	
	$msg = "Here are your updated balances.";
	send_user($account, $settings, $msg);
	
}else{
	$msg = "Parameters missing";
	send_error($msg);
}

function send_error($msg = "") {
		$response["success"] = 0;
        $response["message"] = $msg;
		
		echo json_encode($response);		
    }
	
function send_user($result, $settings, $msg = "") {
		$user;
		$user['email'] = $result['email'];
		$user['current_points'] = $result['current_points'];
		$user['cashout_pending'] = $result['cashout_pending'];
		$user['cashout_amount'] = $result['cashout_amount'];
		$user['referralcount'] = $result['referralcount'];
		$user['banned'] = $result['banned'];
		
		$response["user"] = array();
		array_push($response["user"], $user);
		
		$settings_info;
		$settings_info['conversion_rate'] = $settings['conversion_rate'];
		$settings_info['min_version'] = $settings['min_version'];
		$settings_info['current_version'] = $settings['current_version'];
		$settings_info['cashout_points'] = $settings['cashout_points'];
		$settings_info['referrer_rate'] = $settings['referrer_rate'];
		$settings_info['referred_rate'] = $settings['referred_rate'];
		
		$response["settings"] = array();
		array_push($response["settings"], $settings_info);
		
		$response["success"] = 1;
        $response["message"] = $msg;
		
		echo json_encode($response);		
}

?>