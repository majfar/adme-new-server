<?php 

date_default_timezone_set('America/New_York');

$default_wallpaper = "http://images.fineartamerica.com/images-medium-large-5/cows-in-meadow-christina-rollo.jpg";
	
$db = new mysqli('database.surveycow.co', 'server', 'MajFar123$', 'surveycow') or die("MySQL error: ". mysqli_error());
if ($db->connect_errno) {
	echo "Sorry, this website is experiencing problems.";
	echo "Error: Failed to make a MySQL connection, here is why: \n";
	echo "Errno: " . $db->connect_errno . "\n";
	echo "Error: " . $db->connect_error . "\n";
	exit;
}

$wallpaper_select = $db->prepare("SELECT * FROM wallpaper WHERE id = 1");
$wallpaper_select->execute();

if($wallpaper_select->errno){
	$msg = $default_wallpaper;
	send_response($msg);
	
	$wallpaper_select->close();
	exit;
}

$results = $wallpaper_select->get_result();

if($results->num_rows > 0){
	$wallpaper = $results->fetch_assoc();
	send_response($wallpaper['url']);
}else{
	$msg = $default_wallpaper;
	send_response($msg);
}

$wallpaper_select->close();

function send_response($url) {
	$response["success"] = 1;
	$response["url"] = $url;
	
	echo json_encode($response);		
}

?>